/* SPDX-License-Identifier: LGPL-3.0-or-later OR GPL-2.0 */

/*
 * Copyright (C) 2020 Red Hat, Inc.
 *
 * This file is part of fido2-proxy.
 *
 * fido2-proxy is free software: you can redistribute it and/or
 * modify it under the terms of either:
 *
 *  * the GNU Lesser General Public License as published by the Free
 *    Software Foundation; either version 3 of the License, or (at your
 *    option) any later version.
 *
 * or
 *
 *  * the GNU General Public License as published by the Free
 *    Software Foundation; either version 2 of the License, or (at your
 *    option) any later version.
 *
 * or both in parallel, as here.
 *
 * fido2-proxy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received copies of the GNU General Public License and
 * the GNU Lesser General Public License along with this program.  If
 * not, see https://www.gnu.org/licenses/.
 */

#include "fido2-proxy.h"
#include <stdio.h>
#include <unistd.h>

#define _cleanup_(f) __attribute__((cleanup(f)))

static int
example_pin_cb(int pipe_write) {
	const char *pin = "2345";

	write(pipe_write, pin, 5);
	printf("pin has been written\n");

	return 0;
}

static int
cancel_pin(int pipe_write) {
	return f2p_cancel();
}

int
main() {
	//example usage
	_cleanup_(f2p_creden_free)f2p_creden_t *cred = f2p_creden_init();
	_cleanup_(f2p_assert_free)f2p_assert_t *assert = f2p_assert_init();
	const char *cose_type = "es256";
	const char *rp = "test";
	const char *un = "user name";
	const size_t cdh_len = 32;
	const size_t ui_len = 44;
	const uint8_t cdh_cred[32] = {0x61, 0x2e, 0xef, 0x12, 0xca, 0x69, 0x87, 0xb3, 0x16, 0xc0, 0x61, 0x8b, 0x3a, 0x24, 0x21, 0xa4, 0x3c, 0x7e, 0x59, 0x8c, 0x8a, 0x99, 0x2f, 0x3a, 0x44, 0xe1, 0x85, 0x3c, 0x3f, 0x7d, 0x02, 0x71};
	const uint8_t ui[44] = {0x47, 0x47, 0x68, 0x47, 0x51, 0x55, 0x43, 0x49, 0x2f, 0x50, 0x36, 0x35, 0x31, 0x41, 0x58, 0x73, 0x33, 0x73, 0x7a, 0x56, 0x72, 0x68, 0x76, 0x47, 0x71, 0x65, 0x4d, 0x36, 0x4f, 0x54, 0x70, 0x31, 0x53, 0x5a, 0x2b, 0x78, 0x4e, 0x70, 0x42, 0x58, 0x77, 0x66, 0x6b, 0x3d};
	const uint8_t cdh_assert[32] = {0x99, 0x99, 0x81, 0x59, 0x46, 0x89, 0x1b, 0x01, 0x23, 0x48, 0xd4, 0x2f, 0x90, 0x56, 0xa2, 0x72, 0x90, 0xb3, 0x0c, 0xa8, 0x5a, 0xa7, 0x6a, 0x50, 0x95, 0xf8, 0xbc, 0x15, 0x7b, 0x2f, 0xeb, 0xc9};
	int r;

	printf("---- Register: ----\n\n");
	r = f2p_creden_set_required(cred, cose_type, cdh_cred, cdh_len, rp, ui, ui_len, un);
	if (r < 0)
		return r;

	r = f2p_register(cred, NULL, example_pin_cb);
	if (r < 0)
		return r;

	uint8_t *authdata = f2p_creden_get_authdata(cred);
	size_t authdata_len = f2p_creden_get_authdata_len(cred);
	uint8_t *signature = f2p_creden_get_signature(cred);
	size_t signature_len = f2p_creden_get_signature_len(cred);
	uint8_t *x5c = f2p_creden_get_x5c(cred);
	size_t x5c_len = f2p_creden_get_x5c_len(cred);
	uint8_t *cred_id = f2p_creden_get_credential_id(cred);
	size_t cred_id_len = f2p_creden_get_credential_id_len(cred);
	uint8_t *pubkey = f2p_creden_get_public_key(cred);
	size_t pubkey_len = f2p_creden_get_public_key_len(cred);
	uint8_t *clientdatahash = f2p_creden_get_client_data_hash(cred);
	size_t clientdatahash_len = f2p_creden_get_client_data_hash_len(cred);

	printf("authdata: \n[");
	for (size_t i = 0; i < authdata_len; i++) {
		printf("%d, ", authdata[i]);
	}
	printf("]\n\nsignature:\n[");
	for (size_t i = 0; i < signature_len; i++) {
		printf("%d, ", signature[i]);
	}
	printf("]\n\nx5c:\n[");
	for (size_t i = 0; i < x5c_len; i++) {
		printf("%d, ", x5c[i]);
	}
	printf("]\n\ncredential_id:\n[");
	for (size_t i = 0; i < cred_id_len; i++) {
		printf("%d, ", cred_id[i]);
	}
	printf("]\n\npublic key:\n[");
	for (size_t i = 0; i < pubkey_len; i++) {
		printf("%d, ", pubkey[i]);
	}
	printf("]\n\nclient data hash:\n[");
	for (size_t i = 0; i < clientdatahash_len; i++) {
		printf("%d, ", clientdatahash[i]);
	}
	printf("]\n\n\n---- Cancel Register: ----\n\n");

	r = f2p_register(cred, NULL, cancel_pin);
	if (r != F2P_ERR_CANCEL)
		return r;

	printf("Request cancelled successfully\n\n");

	r = f2p_creden_set_user_name(cred, "name");
	if (r < 0)
		return r;

	r = f2p_creden_set_user_display_name(cred, "display name");
	if (r < 0)
		return r;

	r = f2p_creden_set_user_icon(cred, "icon");
	if (r < 0)
		return r;

	r = f2p_register(cred, NULL, example_pin_cb);
	if (r < 0)
		return r;

	authdata = f2p_creden_get_authdata(cred);
	authdata_len = f2p_creden_get_authdata_len(cred);
	signature = f2p_creden_get_signature(cred);
	signature_len = f2p_creden_get_signature_len(cred);
	x5c = f2p_creden_get_x5c(cred);
	x5c_len = f2p_creden_get_x5c_len(cred);
	cred_id = f2p_creden_get_credential_id(cred);
	cred_id_len = f2p_creden_get_credential_id_len(cred);
	pubkey = f2p_creden_get_public_key(cred);
	pubkey_len = f2p_creden_get_public_key_len(cred);
	clientdatahash = f2p_creden_get_client_data_hash(cred);
	clientdatahash_len = f2p_creden_get_client_data_hash_len(cred);

	printf("authdata: \n[");
	for (size_t i = 0; i < authdata_len; i++) {
		printf("%d, ", authdata[i]);
	}
	printf("]\n\nsignature:\n[");
	for (size_t i = 0; i < signature_len; i++) {
		printf("%d, ", signature[i]);
	}
	printf("]\n\nx5c:\n[");
	for (size_t i = 0; i < x5c_len; i++) {
		printf("%d, ", x5c[i]);
	}
	printf("]\n\ncredential_id:\n[");
	for (size_t i = 0; i < cred_id_len; i++) {
		printf("%d, ", cred_id[i]);
	}
	printf("]\n\npublic key:\n[");
	for (size_t i = 0; i < pubkey_len; i++) {
		printf("%d, ", pubkey[i]);
	}
	printf("]\n\nclient data hash:\n[");
	for (size_t i = 0; i < clientdatahash_len; i++) {
		printf("%d, ", clientdatahash[i]);
	}

	printf("\n\n\n---- Sign: ----\n\n");
	r = f2p_assert_set_required(assert, cdh_assert, cdh_len, rp, cred_id, cred_id_len);
	if (r < 0)
		return r;

	r = f2p_sign(assert, NULL);
	if (r < 0)
		return r;

	authdata = f2p_assert_get_authdata(assert);
	authdata_len = f2p_assert_get_authdata_len(assert);
	size_t count = f2p_assert_get_count(assert);
	clientdatahash = f2p_assert_get_client_data_hash(assert);
	clientdatahash_len = f2p_assert_get_client_data_hash_len(assert);
	uint8_t *hmac_secret = f2p_assert_get_hmac_secret(assert);
	size_t hmac_secret_len = f2p_assert_get_hmac_secret_len(assert);
	uint8_t *user_id = f2p_assert_get_user_id(assert);
	size_t user_id_len = f2p_assert_get_user_id_len(assert);
	char *user_display_name = f2p_assert_get_user_display_name(assert);
	char *user_icon = f2p_assert_get_user_icon(assert);
	char *user_name = f2p_assert_get_user_name(assert);
	signature = f2p_assert_get_signature(assert);
	signature_len = f2p_assert_get_signature_len(assert);
	unsigned signature_count = f2p_assert_get_sig_count(assert);

	printf("authdata: \n[");
	for (size_t i = 0; i < authdata_len; i++) {
		printf("%d, ", authdata[i]);
	}
	printf("]\n\nsignature:\n[");
	for (size_t i = 0; i < signature_len; i++) {
		printf("%d, ", signature[i]);
	}
	printf("]\n\nclient data hash:\n[");
	for (size_t i = 0; i < clientdatahash_len; i++) {
		printf("%d, ", clientdatahash[i]);
	}
	printf("]\n\nhmac secret:\n[");
	for (size_t i = 0; i < hmac_secret_len; i++) {
		printf("%d, ", hmac_secret[i]);
	}
	printf("]\n\nuser id:\n[");
	for (size_t i = 0; i < user_id_len; i++) {
		printf("%d, ", user_id[i]);
	}
	printf("]\n\ncount:\n%lu\n\n", count);
	printf("user display name:\n%s\n\nuser icon:\n%s\n\nuser name:\n%s\n\n", user_display_name, user_icon, user_name);
	printf("signature count: %u\n", signature_count);

	return r;
}
