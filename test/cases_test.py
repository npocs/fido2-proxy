#!/usr/bin/python3
#this file contains the test cases to call in the main test script

import dbus

#-atoms-
#the call
cancel = ["gdbus", "call", "--session", "-d", "org.freedesktop.fido2", "-o", \
            "/org/freedesktop/fido2/Request/0", "-m", "org.freedesktop.fido2.Request.Cancel"]
#required
cose_type1 = "es256"
cose_type2 = "rs256"
cose_type3 = "eddsa"
client_data_hash_cred = [0x61, 0x2e, 0xef, 0x12, 0xca, 0x69, 0x87, 0xb3, 0x16, 0xc0, 0x61, 0x8b, 0x3a, 0x24, 0x21, 0xa4, 0x3c, 0x7e, 0x59, 0x8c, 0x8a, 0x99, 0x2f, 0x3a, 0x44, 0xe1, 0x85, 0x3c, 0x3f, 0x7d, 0x02, 0x71]
client_data_hash_assert = [0x99, 0x99, 0x81, 0x59, 0x46, 0x89, 0x1b, 0x01, 0x23, 0x48, 0xd4, 0x2f, 0x90, 0x56, 0xa2, 0x72, 0x90, 0xb3, 0x0c, 0xa8, 0x5a, 0xa7, 0x6a, 0x50, 0x95, 0xf8, 0xbc, 0x15, 0x7b, 0x2f, 0xeb, 0xc9]
rp0 = "test1jdeioajd"
rp1 = "test2jifwomce"
rp2 = "test3oioewqoh"
user_id0 = [0x47, 0x47, 0x68, 0x47, 0x51, 0x55, 0x43, 0x49, 0x2f, 0x50, 0x36, 0x35, 0x31, 0x41, 0x58, 0x73, 0x33, 0x73, 0x7a, 0x56, 0x72, 0x68, 0x76, 0x47, 0x71, 0x65, 0x4d, 0x36, 0x4f, 0x54, 0x70, 0x31, 0x53, 0x5a, 0x2b, 0x78, 0x4e, 0x70, 0x42, 0x58, 0x77, 0x66, 0x6b, 0x3d]
user_id1 = [0x50, 0x55, 0x4f, 0x37, 0x76, 0x46, 0x43, 0x38, 0x50, 0x55, 0x54, 0x62, 0x67, 0x58, 0x7a, 0x45, 0x79, 0x45, 0x70, 0x79, 0x39, 0x69, 0x64, 0x46, 0x76, 0x31, 0x4b, 0x32, 0x59, 0x35, 0x32, 0x41, 0x54, 0x32, 0x69, 0x35, 0x66, 0x31, 0x38, 0x76, 0x6b, 0x41, 0x6f, 0x3d]
user_id2 = [0x4d, 0x4e, 0x68, 0x76, 0x59, 0x4b, 0x32, 0x61, 0x55, 0x37, 0x6e, 0x5a, 0x37, 0x6c, 0x4f, 0x62, 0x33, 0x45, 0x6c, 0x7a, 0x68, 0x50, 0x42, 0x78, 0x44, 0x61, 0x39, 0x6c, 0x6f, 0x49, 0x6e, 0x35, 0x58, 0x34, 0x45, 0x52, 0x4e, 0x37, 0x45, 0x67, 0x51, 0x32, 0x49, 0x3d]
user_name0 = "japanese"
user_name1 = "american"
user_name2 = "russian"
#optional
user_display_name0 = {"userDisplayName":"佐藤和真"}
user_display_name1 = {"userDisplayName":"Wade Owen Watts"}
user_display_name2 = {"userDisplayName":"Алекс"}
user_icon = {"userIcon":"https://sadanduseless.b-cdn.net/wp-content/uploads/2018/01/rabbit1.jpg"}
extensions0 = {"extensions":dbus.types.Byte(0)}
extensions1 = {"extensions":dbus.types.Byte(1)}
up_on = {"up":dbus.types.Byte(2)}
up_off = {"up":dbus.types.Byte(1)}
uv_on = {"uv":dbus.types.Byte(1)}
uv_off = {"uv":dbus.types.Byte(1)}
rk_on = {"rk":dbus.types.Byte(2)}
rk_off = {"rk":dbus.types.Byte(1)}
count_68 = {"count":dbus.UInt64(68)}
authdata = {"authdata":"{}"}
signature = {"signature":"{}"}
fmt = {"fmt":"{}"}
x5c = {"x5c":"{}"}

#lazy
#basic_cred = test_body + function_credential
#basic_assert = test_body + function_assertion + client_data_hash_assert

# TODO select cose type as a program argument
#my key doesn't support other than cose_type1
req0 = []
req0.append([cose_type1, client_data_hash_cred, rp0, user_id0, user_name0, {}])
#req0.append([cose_type1, client_data_hash_cred, rp1, user_id1, user_name1, {}])
#req0.append([cose_type1, client_data_hash_cred, rp2, user_id2, user_name2, {}])

opt0 = []
opt0.append(user_display_name0)
opt0.append(dict(user_icon, **user_display_name0))
opt0.append(extensions0)
opt0.append(extensions1)
opt0.append(dict(extensions0, **user_display_name0))
opt0.append(dict(extensions1, **user_display_name0))

opt1 = []
opt1.append(user_display_name1)
opt1.append(dict(user_icon, **user_display_name1))
opt1.append(extensions0)
opt1.append(extensions1)
opt1.append(dict(extensions0, **user_display_name1))
opt1.append(dict(extensions1, **user_display_name1))

opt2 = []
opt2.append(user_display_name2)
opt2.append(dict(user_icon, ** user_display_name2))
opt2.append(extensions0)
opt2.append(extensions1)
opt2.append(dict(extensions0, **user_display_name2))
opt2.append(dict(extensions1, **user_display_name2))

#-test cases-
#credention tests
tc_cred_basic = []
tc_cred_advanced = []
for x in req0:
    tc_cred_basic.append(x)
    tmp_dic = {**authdata, **signature, **x5c, **fmt}
    tc_cred_basic.append(x[:-1] + [tmp_dic])

#for x in opt0:
#    for y in req0:
#        tc_cred_basic.append(y[:-1] + [x])
#        tmp_dic = {**authdata, **signature, **x5c, **fmt}
#        tc_cred_basic.append(y[:-1] + [tmp_dic])
#for x in opt1:
#    for y in req0:
#        tc_cred_basic.append(y[:-1] + [x])
#        tmp_dic = {**authdata, **signature, **x5c, **fmt}
#        tc_cred_basic.append(y[:-1] + [tmp_dic])
#for x in opt2:
#    for y in req0:
#        tc_cred_basic.append(y[:-1] + [x])
#        tmp_dic = {**authdata, **signature, **x5c, **fmt}
#        tc_cred_basic.append(y[:-1] + [tmp_dic])

#assert tests
tc_assert = []
aopt = []
aopt.append(dict(up_on, **uv_on, **count_68))
