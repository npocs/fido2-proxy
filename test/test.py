#!/usr/bin/python3
#integration test for fido2-proxy

import subprocess
import time
import os
import cases_test as cases
import dbus
from dbus.mainloop.glib import DBusGMainLoop
from gi.repository import GLib
import traceback

#--VARIABLES--
DBusGMainLoop(set_as_default=True)
user_session = dbus.SessionBus()
loop = GLib.MainLoop()
test_counter = 0
passed_counter = 0
failed_counter = 0
cred_test = True
pin = b"2345"
connected_req = False
quit_loop = False
send_pin = True

#--FUNCTOINS--
def toHexBytes(array):
    return '[byte {}]'.format(', '.join([str(hex(x)) for x in array]))
def hexToUint16(num1, num2):
    return (num1 << 8) + num2
def request_callback(data, member=None):
    if(member == "PinRequired"):
        if(send_pin):
            os.write(data.take(), pin)
        if(quit_loop):
            loop.quit()
    elif(member == "Completed"):
        global passed_counter
        passed_counter += 1
        s = "TEST #" + str(test_counter+1) + ": "
        print(s.ljust(30, '.') + "\u001b[32;1mPASSED\u001b[0m")
        if(cred_test and not data == "The request was cancelled"):
            if(test_counter % 2 == 0):
                tmp = cases.tc_cred_basic[test_counter+1][-1]
                tmp['authdata'] = data.get('authData')
                tmp['signature'] = data.get('signature')
                tmp['x5c'] = data.get('x5c')
                tmp['fmt'] = data.get('fmt')
            #setting assertion
            authdata = data.get('authData').copy()
            credential_id_len = hexToUint16(int(authdata[55]), int(authdata[56]))
            credential_id = data.get('authData')[57:57+credential_id_len]
            #rp is @[2]
            cases.tc_assert.append([cases.client_data_hash_assert, cases.tc_cred_basic[test_counter][2], \
                    credential_id, cases.up_off])
            cases.tc_assert.append([cases.client_data_hash_assert, cases.tc_cred_basic[test_counter][2], \
                    credential_id, dict(cases.up_off, **cases.uv_on)])
        loop.quit()
    elif(member == "Error"):
        fail()
        loop.quit()
def fail():
    global failed_counter
    failed_counter += 1
    s = "TEST #" + str(test_counter+1) + ": "
    print(s.ljust(30, '.') + "\u001b[31;1mFAILED\u001b[0m")
    print("- - - - - - - - - - - - - - - failed test case: - - - - - - - - - - - - - - -")
    if(cred_test):
        print(cases.tc_cred_basic[test_counter])
    else:
        print(cases.tc_assert[test_counter])
    traceback.print_exc()

fp_dev = user_session.get_object('org.freedesktop.fido2', '/org/freedesktop/fido2/Device/0')
fp_req = None

#--TEST CASES--
#_credential_
print("---------- TESTING make_credential ----------")
for case in cases.tc_cred_basic:
    try:
        req_obj = fp_dev.MakeCredential(*case, dbus_interface="org.freedesktop.fido2.Device", byte_arrays=True)
        fp_req = user_session.get_object('org.freedesktop.fido2', '/org/freedesktop/fido2/Request/0')
        if(not connected_req):
            fp_req.connect_to_signal(None, request_callback, member_keyword="member")
            connected_req = True

        loop.run()
    except:
        fail()
    test_counter += 1

#_assert_
print()
print("---------- TESTING get_assertion ----------")
cred_test = False
test_counter = 0
for case in cases.tc_assert:
    try:
        fp_dev.GetAssertion(*case, dbus_interface="org.freedesktop.fido2.Device", byte_arrays=True)
        loop.run()
    except:
        fail()
    test_counter += 1

test_counter = 0
#_set count_
print()
print("---------- TESTING set count ----------")
try:
    fp_dev.GetAssertion(*cases.tc_assert[0][:-1], dict(cases.up_off, **cases.uv_on, **cases.count_68), \
        dbus_interface="org.freedesktop.fido2.Device", byte_arrays=True)
    loop.run()
except:
    fail()

test_counter = 0

#_cancel_
print()
print("---------- TESTING cancel ----------")
for _ in range(50):
    #cred_test = False
    #quit_loop = False
    #send_pin = True
    #try:
    #    req_int = fp_dev.GetAssertion(*cases.tc_assert[0][:-1], cases.up_on, \
    #        dbus_interface="org.freedesktop.fido2.Device", byte_arrays=True)
    #    subprocess.call(cases.cancel, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    #    loop.run()
    #except:
    #    fail()

    #test_counter += 1

    ##cancel before sending pin
    #try:
    #    req_int = fp_dev.GetAssertion(*cases.tc_assert[0][:-1], dict(cases.up_on, **cases.uv_on), \
    #        dbus_interface="org.freedesktop.fido2.Device", byte_arrays=True)
    #    subprocess.call(cases.cancel, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    #    loop.run()
    #except:
    #    fail()

    #test_counter += 1

    quit_loop = False
    cred_test = True
    send_pin = False
    #cancel before sending pin
    try:
        fp_dev.MakeCredential(*cases.tc_cred_basic[0], dbus_interface="org.freedesktop.fido2.Device", \
                byte_arrays=True)
        fp_req = user_session.get_object('org.freedesktop.fido2', '/org/freedesktop/fido2/Request/0')
        fp_req.connect_to_signal(None, request_callback, member_keyword="member")
        subprocess.call(cases.cancel, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        print('Sending cancel')
        loop.run()
    except:
        fail()

    time.sleep(1)
    test_counter += 1

    quit_loop = True
    send_pin = True
    #cancel after sending pin
    try:
        fp_dev.MakeCredential(*cases.tc_cred_basic[0], dbus_interface="org.freedesktop.fido2.Device", \
                byte_arrays=True)
        loop.run()
        time.sleep(1)
        subprocess.call(cases.cancel, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        loop.run()
    except:
        fail()

    time.sleep(1)
    test_counter += 1

    quit_loop = False
    send_pin = False
    #cancel before sending pin
    try:
        fp_dev.MakeCredential(*cases.tc_cred_basic[0], dbus_interface="org.freedesktop.fido2.Device", \
                byte_arrays=True)
        subprocess.call(cases.cancel, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        loop.run()
    except:
        fail()

    time.sleep(1)
    test_counter += 1

#--RESULTS--
print()
print("============= TEST RESULTS =============")
print("PASSED tests: \u001b[32;1m" + str(passed_counter) + "\u001b[0m    |    " + \
        "FAILED tests: \u001b[31;1m" + str(failed_counter) + "\u001b[0m")
print("========================================")
