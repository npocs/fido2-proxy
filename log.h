/*
 * Copyright (C) 2020 Red Hat, Inc.
 *
 * This file is part of fido2-proxy.
 *
 * fido2-proxy is free software: you can redistribute it and/or
 * modify it under the terms of either:
 *
 *  * the GNU Lesser General Public License as published by the Free
 *    Software Foundation; either version 3 of the License, or (at your
 *    option) any later version.
 *
 * or
 *
 *  * the GNU General Public License as published by the Free
 *    Software Foundation; either version 2 of the License, or (at your
 *    option) any later version.
 *
 * or both in parallel, as here.
 *
 * fido2-proxy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received copies of the GNU General Public License and
 * the GNU Lesser General Public License along with this program.  If
 * not, see https://www.gnu.org/licenses/.
 */

#ifndef FP_LOG_H_
#define FP_LOG_H_

enum {
	FP_LOG_LEVEL_ERROR = 1,
	FP_LOG_LEVEL_WARNING = 2,
	FP_LOG_LEVEL_INFO = 3,
	FP_LOG_LEVEL_DEBUG = 4,
};

#define fp_error(format, ...) \
	fp_log(FP_LOG_LEVEL_ERROR, "%s: " format "\n", __PRETTY_FUNCTION__, ##__VA_ARGS__)

#define fp_warning(format, ...) \
	fp_log(FP_LOG_LEVEL_WARNING, "%s: " format "\n", __PRETTY_FUNCTION__, ##__VA_ARGS__)

#define fp_info(format, ...) \
	fp_log(FP_LOG_LEVEL_INFO, "%s: " format "\n", __PRETTY_FUNCTION__, ##__VA_ARGS__)

#define fp_debug(format, ...) \
	fp_log(FP_LOG_LEVEL_DEBUG, "%s: " format "\n", __PRETTY_FUNCTION__, ##__VA_ARGS__)

#define fp_log_errno(r) \
	fp_log(FP_LOG_LEVEL_DEBUG, "%s: errno %d\n", __PRETTY_FUNCTION__, r)

void fp_set_log_level(int level);
void fp_log(int level, const char  *format, ...);

#endif /* FP_LOG_H_ */
