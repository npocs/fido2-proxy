/* SPDX-License-Identifier: LGPL-3.0-or-later OR GPL-2.0 */

/*
 * Copyright (C) 2020 Red Hat, Inc.
 *
 * This file is part of fido2-proxy.
 *
 * fido2-proxy is free software: you can redistribute it and/or
 * modify it under the terms of either:
 *
 *  * the GNU Lesser General Public License as published by the Free
 *    Software Foundation; either version 3 of the License, or (at your
 *    option) any later version.
 *
 * or
 *
 *  * the GNU General Public License as published by the Free
 *    Software Foundation; either version 2 of the License, or (at your
 *    option) any later version.
 *
 * or both in parallel, as here.
 *
 * fido2-proxy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received copies of the GNU General Public License and
 * the GNU Lesser General Public License along with this program.  If
 * not, see https://www.gnu.org/licenses/.
 */

#include "../fido2-proxy.h"

#define _cleanup_(f) __attribute__((cleanup(f)))
#define streq(a, b) (strcmp((a), (b)) == 0)

static int
parse_cred_response(sd_bus_message *m, f2p_creden_t *cred, int(*pin_cb)(int), sd_bus_message_handler_t err_callback) {
	int r = F2P_ERR_NO_MEMBER;
	const char *member;

	member = sd_bus_message_get_member(m);

	if (streq(member, F2P_SERVER_MEMBER_OK)) {
		r = f2p_register_finish(m, cred);
	} else if (streq(member, F2P_SERVER_MEMBER_PIN)) {
		int fd_write;

		r = sd_bus_message_read_basic(m, 'h', &fd_write);
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = pin_cb(fd_write);
		if (r < 0)
			return r;
	} else if (streq(member, F2P_SERVER_MEMBER_ERR)) {
		//normal error msgs
		if (err_callback)
			err_callback(m, NULL, NULL);
		r = F2P_ERR_SERVER;
	}

	return r;
}

static int
parse_assert_response(sd_bus_message *m, f2p_assert_t *assert, sd_bus_message_handler_t err_callback) {
	int r = F2P_ERR_NO_MEMBER;
	const char *member;

	member = sd_bus_message_get_member(m);

	if (streq(member, F2P_SERVER_MEMBER_OK)) {
		r = f2p_sign_finish(m, assert);
	} else if (streq(member, F2P_SERVER_MEMBER_ERR)) {
		//normal error msgs
		if (err_callback)
			err_callback(m, NULL, NULL);
		r = F2P_ERR_SERVER;
	}

	return r;
}

/* dest should be NULL or allocated memory address */
static int
set_uint8_array(uint8_t **dest, const void *src, size_t len) {
	if (*dest)
		free(*dest);

	*dest = calloc(len, 1);
	if (!*dest)
		return F2P_ERR_INTERNAL;

	if (memcpy(*dest, src, len) == NULL) {
		free(*dest);
		*dest = NULL;
		
		return F2P_ERR_INTERNAL;
	}

	return F2P_OK;
}

/* dest should be NULL or allocated memory address */
static int
set_string(char **dest, const void *src) {
	if (*dest)
		free(*dest);

	*dest = calloc(strlen(src) + 1, 1);
	if (!*dest)
		return F2P_ERR_INTERNAL;

	if (!strcpy(*dest, src)) {
		free(*dest);
		 *dest = NULL;

		return F2P_ERR_INTERNAL;
	}

	return F2P_OK;
}

int
f2p_register(f2p_creden_t *cred, sd_bus_message_handler_t err_callback, f2p_pin_req_callback_t pin_callback) {
	if (!cred || !cred->cose_type || !cred->client_data_hash ||
		!cred->relying_party || !cred->user_id || !cred->user_name || !pin_callback) {

		return F2P_ERR_REQUIRED;
	}

	int r;
	_cleanup_(sd_bus_flush_close_unrefp) sd_bus *user_bus = NULL;
	_cleanup_(sd_bus_message_unrefp) sd_bus_message *m = NULL;
	_cleanup_(sd_bus_message_unrefp) sd_bus_message *reply = NULL;
	_cleanup_(sd_bus_error_free) sd_bus_error error = SD_BUS_ERROR_NULL;

	r = sd_bus_default_user(&user_bus);
	if (r < 0)
		return F2P_ERR_DBUS;

	r = sd_bus_add_match(user_bus, NULL, "type='signal', interface='org.freedesktop.fido2.Request'", NULL, NULL);
	if (r < 0)
		return F2P_ERR_DBUS;

	r = sd_bus_message_new_method_call(user_bus, &m, 
					   F2P_SERVER_DBUS_NAME,
					   F2P_SERVER_DBUS_DEVICE_OBJECT,
					   F2P_SERVER_DBUS_DEVICE_INTERFACE,
					   "MakeCredential");
	if (r < 0)
		return F2P_ERR_DBUS;

	/* REQUIRED ARGUMENTS */
	r = sd_bus_message_append_basic(m, 's', cred->cose_type);
	if (r < 0)
		return F2P_ERR_BUS_MSG;

	r = sd_bus_message_append_array(m, 'y',
					cred->client_data_hash,
					cred->client_data_hash_len);
	if (r < 0)
		return F2P_ERR_BUS_MSG;

	r = sd_bus_message_append_basic(m, 's', cred->relying_party);
	if (r < 0)
		return F2P_ERR_BUS_MSG;

	r = sd_bus_message_append_array(m, 'y',
					cred->user_id,
					cred->user_id_len);
	if (r < 0)
		return F2P_ERR_BUS_MSG;

	r = sd_bus_message_append_basic(m, 's', cred->user_name);
	if (r < 0)
		return F2P_ERR_BUS_MSG;

	/* OPTIONAL ARGUMENTS */
	r = sd_bus_message_open_container(m, SD_BUS_TYPE_ARRAY, "{sv}");
	if (r < 0)
		return F2P_ERR_BUS_MSG;

	if (cred->rk != F2P_ARG_NOT_SET) {
		r = sd_bus_message_open_container(m, SD_BUS_TYPE_DICT_ENTRY, "sv");
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_append_basic(m, 's', F2P_ARG_RK);
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_open_container(m, SD_BUS_TYPE_VARIANT, "y");
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_append_basic(m, 'y', &cred->rk);
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_close_container(m);
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_close_container(m);
		if (r < 0)
			return F2P_ERR_BUS_MSG;
	}

	if (cred->uv != F2P_ARG_NOT_SET) {
		r = sd_bus_message_open_container(m, SD_BUS_TYPE_DICT_ENTRY, "sv");
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_append_basic(m, 's', F2P_ARG_UV);
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_open_container(m, SD_BUS_TYPE_VARIANT, "y");
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_append_basic(m, 'y', &cred->uv);
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_close_container(m);
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_close_container(m);
		if (r < 0)
			return F2P_ERR_BUS_MSG;
	}

	if (cred->extensions != F2P_ARG_NOT_SET) {
		r = sd_bus_message_open_container(m, SD_BUS_TYPE_DICT_ENTRY, "sv");
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_append_basic(m, 's', F2P_ARG_EXTENSIONS);
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_open_container(m, SD_BUS_TYPE_VARIANT, "y");
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_append_basic(m, 'y', &cred->extensions);
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_close_container(m);
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_close_container(m);
		if (r < 0)
			return F2P_ERR_BUS_MSG;
	}

	if (cred->authdata) {
		r = sd_bus_message_open_container(m, SD_BUS_TYPE_DICT_ENTRY, "sv");
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_append_basic(m, 's', F2P_ARG_AUTHDATA);
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_open_container(m, SD_BUS_TYPE_VARIANT, "ay");
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_append_array(m, 'y', cred->authdata, cred->authdata_len);
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_close_container(m);
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_close_container(m);
		if (r < 0)
			return F2P_ERR_BUS_MSG;
	}

	if (cred->x5c) {
		r = sd_bus_message_open_container(m, SD_BUS_TYPE_DICT_ENTRY, "sv");
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_append_basic(m, 's', F2P_ARG_X5C);
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_open_container(m, SD_BUS_TYPE_VARIANT, "ay");
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_append_array(m, 'y', cred->x5c, cred->x5c_len);
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_close_container(m);
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_close_container(m);
		if (r < 0)
			return F2P_ERR_BUS_MSG;
	}

	if (cred->signature) {
		r = sd_bus_message_open_container(m, SD_BUS_TYPE_DICT_ENTRY, "sv");
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_append_basic(m, 's', F2P_ARG_SIGNATURE);
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_open_container(m, SD_BUS_TYPE_VARIANT, "ay");
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_append_array(m, 'y', cred->signature, cred->signature_len);
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_close_container(m);
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_close_container(m);
		if (r < 0)
			return F2P_ERR_BUS_MSG;
	}

	if (cred->fmt) {
		r = sd_bus_message_open_container(m, SD_BUS_TYPE_DICT_ENTRY, "sv");
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_append_basic(m, 's', F2P_ARG_FMT);
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_open_container(m, SD_BUS_TYPE_VARIANT, "s");
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_append_basic(m, 's', cred->fmt);
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_close_container(m);
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_close_container(m);
		if (r < 0)
			return F2P_ERR_BUS_MSG;
	}

	if (cred->user_display_name) {
		r = sd_bus_message_open_container(m, SD_BUS_TYPE_DICT_ENTRY, "sv");
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_append_basic(m, 's', F2P_ARG_USER_DISPLAY_NAME);
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_open_container(m, SD_BUS_TYPE_VARIANT, "s");
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_append_basic(m, 's', cred->user_display_name);
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_close_container(m);
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_close_container(m);
		if (r < 0)
			return F2P_ERR_BUS_MSG;
	}

	if (cred->user_icon) {
		r = sd_bus_message_open_container(m, SD_BUS_TYPE_DICT_ENTRY, "sv");
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_append_basic(m, 's', F2P_ARG_USER_ICON);
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_open_container(m, SD_BUS_TYPE_VARIANT, "s");
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_append_basic(m, 's', cred->user_icon);
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_close_container(m);
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_close_container(m);
		if (r < 0)
			return F2P_ERR_BUS_MSG;
	}

	r = sd_bus_message_close_container(m);
	if (r < 0)
		return F2P_ERR_BUS_MSG;

	r = sd_bus_call(user_bus, m, -1, &error, &reply);
	if (r < 0)
		return F2P_ERR_DBUS;

	for (;;) {
		sd_bus_message_unrefp(&reply);
		r = sd_bus_process(user_bus, &reply);
		if (r < 0)
			return F2P_ERR_DBUS;
		else if (r == 0) {
			r = sd_bus_wait(user_bus, 0);
			if (r < 0)
				return F2P_ERR_DBUS;
		} else {
			if (streq(sd_bus_message_get_interface(reply), F2P_SERVER_DBUS_REQUEST_INTERFACE)) {
				if (streq(sd_bus_message_get_member(reply), F2P_SERVER_MEMBER_PIN)) {
					r = parse_cred_response(reply, cred, pin_callback, err_callback);
					if (r < 0)
						return r;
				} else {
					r = parse_cred_response(reply, cred, pin_callback, err_callback);
					return r;
				}
				//use-case = service is not available (this will loop forever)
			}
		}
	}

	return F2P_OK;
}

int
f2p_register_finish(sd_bus_message *m, f2p_creden_t *cred) {
	int r;

	r = sd_bus_message_enter_container(m, 'a', "{sv}");
	if (r < 0)
		return F2P_ERR_DBUS_MSG_READ;

	for (;;) {
		const char *label;

		r = sd_bus_message_enter_container(m, 'e', "sv");
		if (r < 0)
			return F2P_ERR_DBUS_MSG_READ;
		if (r == 0)
			break;

		r = sd_bus_message_read_basic(m, 's', &label);
		if (r < 0)
			return F2P_ERR_DBUS_MSG_READ;

		if (streq(label, "authData")) {
			r = sd_bus_message_enter_container(m, 'v', "ay");
			if (r < 0)
				return F2P_ERR_DBUS_MSG_READ;

			const void *authdata;

			r = sd_bus_message_read_array(m, 'y', &authdata, &(cred->authdata_len));
			if (r < 0)
				return F2P_ERR_DBUS_MSG_READ;

			r = set_uint8_array(&cred->authdata, authdata, cred->authdata_len);
			if (r < 0) {
				return r;
			}

			r = sd_bus_message_exit_container(m);
			if (r < 0)
				return F2P_ERR_DBUS_MSG_READ;
		} else if (streq(label, "signature")) {
			r = sd_bus_message_enter_container(m, 'v', "ay");
			if (r < 0)
				return F2P_ERR_DBUS_MSG_READ;

			const void *signature;

			r = sd_bus_message_read_array(m, 'y', &signature, &(cred->signature_len));
			if (r < 0)
				return F2P_ERR_DBUS_MSG_READ;

			r = set_uint8_array(&cred->signature, signature, cred->signature_len);
			if (r < 0)
				return r;

			r = sd_bus_message_exit_container(m);
			if (r < 0)
				return F2P_ERR_DBUS_MSG_READ;
		} else if (streq(label, "credentialID")) {
			r = sd_bus_message_enter_container(m, 'v', "ay");
			if (r < 0)
				return F2P_ERR_DBUS_MSG_READ;

			const void *credential_id;

			r = sd_bus_message_read_array(m, 'y', &credential_id, &(cred->credential_id_len));
			if (r < 0)
				return F2P_ERR_DBUS_MSG_READ;

			r = set_uint8_array(&cred->credential_id, credential_id, cred->credential_id_len);
			if (r < 0)
				return r;

			r = sd_bus_message_exit_container(m);
			if (r < 0)
				return F2P_ERR_DBUS_MSG_READ;
		} else if (streq(label, "publicKey")) {
			r = sd_bus_message_enter_container(m, 'v', "ay");
			if (r < 0)
				return F2P_ERR_DBUS_MSG_READ;

			const void *public_key;

			r = sd_bus_message_read_array(m, 'y', &public_key, &(cred->public_key_len));
			if (r < 0)
				return F2P_ERR_DBUS_MSG_READ;

			r = set_uint8_array(&cred->public_key, public_key, cred->public_key_len);
			if (r < 0)
				return r;

			r = sd_bus_message_exit_container(m);
			if (r < 0)
				return F2P_ERR_DBUS_MSG_READ;
		} else if (streq(label, "x5c")) {
			r = sd_bus_message_enter_container(m, 'v', "ay");
			if (r < 0)
				return F2P_ERR_DBUS_MSG_READ;

			const void *x5c;

			r = sd_bus_message_read_array(m, 'y', &x5c, &(cred->x5c_len));
			if (r < 0)
				return F2P_ERR_DBUS_MSG_READ;

			r = set_uint8_array(&cred->x5c, x5c, cred->x5c_len);
			if (r < 0)
				return r;

			r = sd_bus_message_exit_container(m);
			if (r < 0)
				return F2P_ERR_DBUS_MSG_READ;
		} else if (streq(label, "fmt")) {
			r = sd_bus_message_enter_container(m, 'v', "s");
			if (r < 0)
				return F2P_ERR_DBUS_MSG_READ;

			const void *fmt;

			r = sd_bus_message_read_basic(m, 's', &fmt);
			if (r < 0)
				return F2P_ERR_DBUS_MSG_READ;

			r = set_string(&cred->fmt, fmt);
			if (r < 0)
				return r;

			r = sd_bus_message_exit_container(m);
			if (r < 0)
				return F2P_ERR_DBUS_MSG_READ;
		} else if (streq(label, "clientdataHash")) {
			r = sd_bus_message_enter_container(m, 'v', "ay");
			if (r < 0)
				return F2P_ERR_DBUS_MSG_READ;

			const void *client_data_hash;

			r = sd_bus_message_read_array(m, 'y', &client_data_hash, &(cred->client_data_hash_len));
			if (r < 0)
				return F2P_ERR_DBUS_MSG_READ;

			r = set_uint8_array(&cred->client_data_hash, client_data_hash, cred->client_data_hash_len);
			if (r < 0)
				return r;

			r = sd_bus_message_exit_container(m);
			if (r < 0)
				return F2P_ERR_DBUS_MSG_READ;
		}

		r = sd_bus_message_exit_container(m);
		if (r < 0)
			return F2P_ERR_DBUS_MSG_READ;
	}

	return F2P_OK;
}

int
f2p_sign(f2p_assert_t *assert, sd_bus_message_handler_t err_callback) {
	if (!assert || !assert->client_data_hash || !assert->relying_party || !assert->allow_credential) {
		return F2P_ERR_REQUIRED;
	}

	int r;
	_cleanup_(sd_bus_flush_close_unrefp) sd_bus *user_bus = NULL;
	_cleanup_(sd_bus_message_unrefp) sd_bus_message *m = NULL;
	_cleanup_(sd_bus_message_unrefp) sd_bus_message *reply = NULL;
	_cleanup_(sd_bus_error_free) sd_bus_error error = SD_BUS_ERROR_NULL;

	r = sd_bus_default_user(&user_bus);
	if (r < 0)
		return F2P_ERR_DBUS;

	r = sd_bus_add_match(user_bus, NULL, "type='signal', interface='org.freedesktop.fido2.Request'", NULL, NULL);
	if (r < 0)
		return F2P_ERR_DBUS;

	r = sd_bus_message_new_method_call(user_bus, &m, 
					   F2P_SERVER_DBUS_NAME,
					   F2P_SERVER_DBUS_DEVICE_OBJECT,
					   F2P_SERVER_DBUS_DEVICE_INTERFACE,
					   "GetAssertion");
	if (r < 0)
		return F2P_ERR_DBUS;

	/* REQUIRED ARGUMENTS */
	r = sd_bus_message_append_array(m, 'y',
					assert->client_data_hash,
					assert->client_data_hash_len);
	if (r < 0)
		return F2P_ERR_BUS_MSG;

	r = sd_bus_message_append_basic(m, 's', assert->relying_party);
	if (r < 0)
		return F2P_ERR_BUS_MSG;
	
	r = sd_bus_message_append_array(m, 'y',
					assert->allow_credential,
					assert->allow_credential_len);
	if (r < 0)
		return F2P_ERR_BUS_MSG;

	/* OPTIONAL ARGUMENTS */
	r = sd_bus_message_open_container(m, SD_BUS_TYPE_ARRAY, "{sv}");
	if (r < 0)
		return F2P_ERR_BUS_MSG;

	if (assert->up != F2P_ARG_NOT_SET) {
		r = sd_bus_message_open_container(m, SD_BUS_TYPE_DICT_ENTRY, "sv");
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_append_basic(m, 's', F2P_ARG_UP);
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_open_container(m, SD_BUS_TYPE_VARIANT, "y");
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_append_basic(m, 'y', &assert->up);
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_close_container(m);
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_close_container(m);
		if (r < 0)
			return F2P_ERR_BUS_MSG;
	}

	if (assert->uv != F2P_ARG_NOT_SET) {
		r = sd_bus_message_open_container(m, SD_BUS_TYPE_DICT_ENTRY, "sv");
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_append_basic(m, 's', F2P_ARG_UV);
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_open_container(m, SD_BUS_TYPE_VARIANT, "y");
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_append_basic(m, 'y', &assert->uv);
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_close_container(m);
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_close_container(m);
		if (r < 0)
			return F2P_ERR_BUS_MSG;
	}

	if (assert->extensions != F2P_ARG_NOT_SET) {
		r = sd_bus_message_open_container(m, SD_BUS_TYPE_DICT_ENTRY, "sv");
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_append_basic(m, 's', F2P_ARG_EXTENSIONS);
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_open_container(m, SD_BUS_TYPE_VARIANT, "y");
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_append_basic(m, 'y', &assert->extensions);
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_close_container(m);
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_close_container(m);
		if (r < 0)
			return F2P_ERR_BUS_MSG;
	}

	if (assert->count) {
		r = sd_bus_message_open_container(m, SD_BUS_TYPE_DICT_ENTRY, "sv");
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_append_basic(m, 's', F2P_ARG_COUNT);
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_open_container(m, SD_BUS_TYPE_VARIANT, "t");
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_append_basic(m, 't', &assert->count);
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_close_container(m);
		if (r < 0)
			return F2P_ERR_BUS_MSG;

		r = sd_bus_message_close_container(m);
		if (r < 0)
			return F2P_ERR_BUS_MSG;
	}

	r = sd_bus_message_close_container(m);
	if (r < 0)
		return F2P_ERR_BUS_MSG;

	r = sd_bus_call(user_bus, m, -1, &error, &reply);
	if (r < 0)
		return F2P_ERR_DBUS;

	for (;;) {
		sd_bus_message_unrefp(&reply);
		r = sd_bus_process(user_bus, &reply);
		if (r < 0)
			return F2P_ERR_DBUS;
		else if (r == 0) {
			r = sd_bus_wait(user_bus, 0);
			if (r < 0)
				return F2P_ERR_DBUS;
		} else {
			if (streq(sd_bus_message_get_interface(reply), F2P_SERVER_DBUS_REQUEST_INTERFACE)) {
				r = parse_assert_response(reply, assert, err_callback);
				break;
				//use-case = service is not available (this will loop forever)
			}
		}
	}

	return F2P_OK;
}

int
f2p_sign_finish(sd_bus_message *m, f2p_assert_t *assert) {
	int r;

	r = sd_bus_message_enter_container(m, 'a', "{sv}");
	if (r < 0)
		return F2P_ERR_DBUS_MSG_READ;

	for (;;) {
		const char *label;

		r = sd_bus_message_enter_container(m, 'e', "sv");
		if (r < 0)
			return F2P_ERR_DBUS_MSG_READ;
		if (r == 0)
			break;

		r = sd_bus_message_read_basic(m, 's', &label);
		if (r < 0)
			return F2P_ERR_DBUS_MSG_READ;

		if (streq(label, F2P_ARG_AUTHDATA_S)) {
			r = sd_bus_message_enter_container(m, 'v', "ay");
			if (r < 0)
				return F2P_ERR_DBUS_MSG_READ;

			const void *authdata;

			r = sd_bus_message_read_array(m, 'y', &authdata, &(assert->authdata_len));
			if (r < 0)
				return F2P_ERR_DBUS_MSG_READ;

			r = set_uint8_array(&assert->authdata, authdata, assert->authdata_len);
			if (r < 0)
				return r;

			r = sd_bus_message_exit_container(m);
			if (r < 0)
				return F2P_ERR_DBUS_MSG_READ;
		} else if (streq(label, F2P_ARG_COUNT)) {
			r = sd_bus_message_enter_container(m, 'v', "t");
			if (r < 0)
				return F2P_ERR_DBUS_MSG_READ;

			r = sd_bus_message_read_basic(m, 't', &assert->count);
			if (r < 0)
				return F2P_ERR_DBUS_MSG_READ;

			r = sd_bus_message_exit_container(m);
			if (r < 0)
				return F2P_ERR_DBUS_MSG_READ;
		} else if (streq(label, F2P_ARG_CLIENT_DATA_HASH)) {
			r = sd_bus_message_enter_container(m, 'v', "ay");
			if (r < 0)
				return F2P_ERR_DBUS_MSG_READ;

			const void *client_data_hash;

			r = sd_bus_message_read_array(m, 'y', &client_data_hash, &(assert->client_data_hash_len));
			if (r < 0)
				return F2P_ERR_DBUS_MSG_READ;

			r = set_uint8_array(&assert->client_data_hash, client_data_hash, assert->client_data_hash_len);
			if (r < 0)
				return r;

			r = sd_bus_message_exit_container(m);
			if (r < 0)
				return F2P_ERR_DBUS_MSG_READ;
		} else if (streq(label, F2P_ARG_HMAC_SECRET)) {
			r = sd_bus_message_enter_container(m, 'v', "ay");
			if (r < 0)
				return F2P_ERR_DBUS_MSG_READ;

			const void *hmac_secret;

			r = sd_bus_message_read_array(m, 'y', &hmac_secret, &(assert->hmac_secret_len));
			if (r < 0)
				return F2P_ERR_DBUS_MSG_READ;

			r = set_uint8_array(&assert->hmac_secret, hmac_secret, assert->hmac_secret_len);
			if (r < 0)
				return r;

			r = sd_bus_message_exit_container(m);
			if (r < 0)
				return F2P_ERR_DBUS_MSG_READ;
		} else if (streq(label, F2P_ARG_USER_ID)) {
			r = sd_bus_message_enter_container(m, 'v', "ay");
			if (r < 0)
				return F2P_ERR_DBUS_MSG_READ;

			const void *user_id;

			r = sd_bus_message_read_array(m, 'y', &user_id, &(assert->user_id_len));
			if (r < 0)
				return F2P_ERR_DBUS_MSG_READ;

			r = set_uint8_array(&assert->user_id, user_id, assert->user_id_len);
			if (r < 0)
				return r;

			r = sd_bus_message_exit_container(m);
			if (r < 0)
				return F2P_ERR_DBUS_MSG_READ;
		} else if (streq(label, F2P_ARG_USER_DISPLAY_NAME)) {
			r = sd_bus_message_enter_container(m, 'v', "s");
			if (r < 0)
				return F2P_ERR_DBUS_MSG_READ;

			const void *user_display_name;

			r = sd_bus_message_read_basic(m, 's', &user_display_name);
			if (r < 0)
				return F2P_ERR_DBUS_MSG_READ;

			r = set_string(&assert->user_display_name, user_display_name);
			if (r < 0)
				return r;

			r = sd_bus_message_exit_container(m);
			if (r < 0)
				return F2P_ERR_DBUS_MSG_READ;
		} else if (streq(label, F2P_ARG_USER_ICON)) {
			r = sd_bus_message_enter_container(m, 'v', "s");
			if (r < 0)
				return F2P_ERR_DBUS_MSG_READ;

			const void *user_icon;

			r = sd_bus_message_read_basic(m, 's', &user_icon);
			if (r < 0)
				return F2P_ERR_DBUS_MSG_READ;

			r = set_string(&assert->user_icon, user_icon);
			if (r < 0)
				return r;

			r = sd_bus_message_exit_container(m);
			if (r < 0)
				return F2P_ERR_DBUS_MSG_READ;
		} else if (streq(label, F2P_ARG_USER_NAME)) {
			r = sd_bus_message_enter_container(m, 'v', "s");
			if (r < 0)
				return F2P_ERR_DBUS_MSG_READ;

			const void *user_name;

			r = sd_bus_message_read_basic(m, 's', &user_name);
			if (r < 0)
				return F2P_ERR_DBUS_MSG_READ;

			r = set_string(&assert->user_name, user_name);
			if (r < 0)
				return r;

			r = sd_bus_message_exit_container(m);
			if (r < 0)
				return F2P_ERR_DBUS_MSG_READ;
		} else if (streq(label, F2P_ARG_SIGNATURE)) {
			r = sd_bus_message_enter_container(m, 'v', "ay");
			if (r < 0)
				return F2P_ERR_DBUS_MSG_READ;

			const void *signature;

			r = sd_bus_message_read_array(m, 'y', &signature, &(assert->signature_len));
			if (r < 0)
				return F2P_ERR_DBUS_MSG_READ;

			r = set_uint8_array(&assert->signature, signature, assert->signature_len);
			if (r < 0)
				return r;

			r = sd_bus_message_exit_container(m);
			if (r < 0)
				return F2P_ERR_DBUS_MSG_READ;
		} else if (streq(label, F2P_ARG_SIGNATURE_COUNT)) {
			r = sd_bus_message_enter_container(m, 'v', "u");
			if (r < 0)
				return F2P_ERR_DBUS_MSG_READ;

			r = sd_bus_message_read_basic(m, 'u', &assert->sig_count);
			if (r < 0)
				return F2P_ERR_DBUS_MSG_READ;

			r = sd_bus_message_exit_container(m);
			if (r < 0)
				return F2P_ERR_DBUS_MSG_READ;
		}

		r = sd_bus_message_exit_container(m);
		if (r < 0)
			return F2P_ERR_DBUS_MSG_READ;
	}

	return r;
}

int
f2p_cancel(void) {
	int r;
	_cleanup_(sd_bus_flush_close_unrefp) sd_bus *user_bus = NULL;
	_cleanup_(sd_bus_message_unrefp) sd_bus_message *m = NULL;
	_cleanup_(sd_bus_message_unrefp) sd_bus_message *reply = NULL;
	_cleanup_(sd_bus_error_free) sd_bus_error error = SD_BUS_ERROR_NULL;

	r = sd_bus_default_user(&user_bus);
	if (r < 0)
		return F2P_ERR_DBUS;

	r = sd_bus_message_new_method_call(user_bus, &m, 
					   F2P_SERVER_DBUS_NAME,
					   F2P_SERVER_DBUS_REQUEST_OBJECT,
					   F2P_SERVER_DBUS_REQUEST_INTERFACE,
					   "Cancel");
	if (r < 0)
		return F2P_ERR_DBUS;

	r = sd_bus_call(user_bus, m, -1, &error, &reply);
	if (r < 0)
		return F2P_ERR_DBUS;

	return F2P_ERR_CANCEL;
}
