/* SPDX-License-Identifier: LGPL-3.0-or-later OR GPL-2.0 */

/*
 * Copyright (C) 2020 Red Hat, Inc.
 *
 * This file is part of fido2-proxy.
 *
 * fido2-proxy is free software: you can redistribute it and/or
 * modify it under the terms of either:
 *
 *  * the GNU Lesser General Public License as published by the Free
 *    Software Foundation; either version 3 of the License, or (at your
 *    option) any later version.
 *
 * or
 *
 *  * the GNU General Public License as published by the Free
 *    Software Foundation; either version 2 of the License, or (at your
 *    option) any later version.
 *
 * or both in parallel, as here.
 *
 * fido2-proxy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received copies of the GNU General Public License and
 * the GNU Lesser General Public License along with this program.  If
 * not, see https://www.gnu.org/licenses/.
 */

#ifndef _FIDO2_PROXY_ERR_H_
#define _FIDO2_PROXY_ERR_H_

#define F2P_OK				0
#define F2P_ERR_DBUS			-1
#define F2P_ERR_SERVER			-2
#define F2P_ERR_BUS_MSG			-3
#define F2P_ERR_PIN_REQ			-4
#define F2P_ERR_PIN_NOT_SET		-5
#define F2P_ERR_NO_DEVICE		-6
#define F2P_ERR_EVENT			-7
#define F2P_ERR_DBUS_MSG_READ		-8
#define F2P_ERR_INTERNAL		-9
#define F2P_ERR_NO_MEMBER		-10
#define F2P_ERR_CANCEL			-11
#define F2P_ERR_INVALID_ARGUMENT	-22
#define F2P_ERR_TIMEOUT			-62
#define F2P_ERR_REQUIRED		-126

#endif /* _FIDO2_PROXY_ERR_H_ */
