/* SPDX-License-Identifier: LGPL-3.0-or-later OR GPL-2.0 */

/*
 * Copyright (C) 2020 Red Hat, Inc.
 *
 * This file is part of fido2-proxy.
 *
 * fido2-proxy is free software: you can redistribute it and/or
 * modify it under the terms of either:
 *
 *  * the GNU Lesser General Public License as published by the Free
 *    Software Foundation; either version 3 of the License, or (at your
 *    option) any later version.
 *
 * or
 *
 *  * the GNU General Public License as published by the Free
 *    Software Foundation; either version 2 of the License, or (at your
 *    option) any later version.
 *
 * or both in parallel, as here.
 *
 * fido2-proxy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received copies of the GNU General Public License and
 * the GNU Lesser General Public License along with this program.  If
 * not, see https://www.gnu.org/licenses/.
 */

#include "config.h"

#include <fido.h>
#include <libudev.h>
#include <systemd/sd-bus.h>
#include <systemd/sd-event.h>
#include <sys/mman.h>

#include <errno.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <unistd.h>
#include <fcntl.h>

#include "fido_check.h"
#include "log.h"

#define FP_SERVICE_NAME "org.freedesktop.fido2"
#define FP_REQUEST_PATH "/org/freedesktop/fido2/Request"
#define FP_REQUEST_INTERFACE "org.freedesktop.fido2.Request"

#define FP_DEVICE_PATH "/org/freedesktop/fido2/Device"
#define FP_DEVICE_INTERFACE "org.freedesktop.fido2.Device"

#define streq(a, b) (strcmp((a), (b)) == 0)

#define _cleanup_(f) __attribute__((cleanup(f)))
static inline void freep(void *p) {
        free(*(void**) p);
}

#define _cleanup_free_ _cleanup_(freep)

static pthread_t worker_thread;
static pthread_mutex_t worker_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t worker_cond = PTHREAD_COND_INITIALIZER;
static pthread_mutex_t cancel_mutex = PTHREAD_MUTEX_INITIALIZER;

static _Thread_local bool not_cancel;

struct fp_request {
	sd_bus_slot *slot;
	char *object_path;
	void *data;
	void (*data_free)(void *);
	int (*dispatch)(sd_bus *, const char *, void *);
	struct fp_request *next;
};

static void
fp_request_free(struct fp_request *request)
{
	if (!request)
		return;

	free(request->object_path);
	sd_bus_slot_unref(request->slot);
	if (request->data_free)
		request->data_free(request->data);
	free(request);
}

static inline void
fp_request_freep(void *p)
{
	struct fp_request *request = *(struct fp_request **)p;
	fp_request_free(request);
}

struct fp_requests {
	struct fp_request *head;
	struct fp_request *tail;
	size_t n_requests;
};

struct fp_device;

struct fp_server {
	sd_bus *bus;
	sd_bus_slot *slot;
	struct fp_device *devices;
	struct fp_requests requests;
	size_t n_devices;
	struct udev *udev;
	struct udev_monitor *udev_monitor;
	sd_event_source *udev_monitor_source;
	sd_event *event;
};

struct fp_device {
	struct fp_server *server;
	sd_bus_slot *slot;
	char *path;
	char *object_path;
	struct fp_device *prev;
	struct fp_device *next;
};

struct make_cred_closure {
	fido_dev_t *dev;
	fido_cred_t *cred;
	int pp[2];
};

struct get_assert_closure {
	fido_dev_t *dev;
	fido_assert_t *assert;
	int pp[2];
	bool uv;
};

static void signal_handler(int i) {
	not_cancel = false;
	pthread_mutex_unlock(&cancel_mutex);
	signal(SIGUSR1, signal_handler);

	pthread_mutex_lock(&cancel_mutex);
}

static int
request_make_cred(sd_bus *bus, const char *object_path, void *data)
{
	struct make_cred_closure *closure = data;
	_cleanup_(sd_bus_message_unrefp) sd_bus_message *m = NULL;
	int r;
	char pin[64];

	memset(pin, 0, 64);

	if (not_cancel) {
		r = fido_dev_make_cred(closure->dev, closure->cred, NULL);
	}
	if (r == FIDO_ERR_PIN_REQUIRED) {
		r = sd_bus_message_new_signal(bus, &m, object_path,
					      FP_REQUEST_INTERFACE,
					      "PinRequired");
		if (r < 0) {
			fp_log_errno(-r);
			goto err_cleanup;
		}

		r = pipe2(closure->pp, O_CLOEXEC);
		if (r < 0) {
			fp_log_errno(-r);
			goto err_cleanup;
		}

		r = sd_bus_message_append(m, "h", closure->pp[1]);
		if (r < 0) {
			fp_log_errno(-r);
			goto err_cleanup;
		}

		r = sd_bus_send(bus, m, NULL);
		if (r < 0) {
			fp_log_errno(-r);
			goto err_cleanup;
		}

		if (not_cancel) {
			r = read(closure->pp[0], pin, 64);
		}
		if (not_cancel && r < 0) {
			fp_log_errno(-errno);
			goto err_cleanup;
		}

		if (not_cancel) {
			r = fido_dev_make_cred(closure->dev, closure->cred, pin);
		}

		close(closure->pp[0]);
		close(closure->pp[1]);
	}

	if (!not_cancel) {
		r = sd_bus_message_new_signal(bus, &m, object_path,
					      FP_REQUEST_INTERFACE,
					      "Completed");
		if (r < 0) {
			fp_log_errno(-r);
			goto err_cleanup;
		}

		r = sd_bus_message_append(m, "s", "The request was cancelled");
		if (r < 0) {
			fp_log_errno(-r);
			goto err_cleanup;
		}
	} else if (r != FIDO_OK) {
		fp_error("failed to make credential: %s", fido_strerr(r));
		r = sd_bus_message_new_signal(bus, &m, object_path,
					      FP_REQUEST_INTERFACE,
					      "Error");
		if (r < 0) {
			fp_log_errno(-r);
			goto err_cleanup;
		}
		r = sd_bus_message_append(m, "u", EINVAL);
		if (r < 0) {
			fp_log_errno(-r);
			goto err_cleanup;
		}
		r = sd_bus_message_append(m, "s", "Failed to make cred");
		if (r < 0) {
			fp_log_errno(-r);
			goto err_cleanup;
		}
	} else {
		r = sd_bus_message_new_signal(bus, &m, object_path,
					      FP_REQUEST_INTERFACE,
					      "Completed");
		if (r < 0) {
			fp_log_errno(-r);
			goto err_cleanup;
		}
		r = sd_bus_message_open_container(m, SD_BUS_TYPE_ARRAY, "{sv}");
		if (r < 0) {
			fp_log_errno(-r);
			goto err_cleanup;
		}
		r = sd_bus_message_open_container(m, SD_BUS_TYPE_DICT_ENTRY, "sv");
		if (r < 0) {
			fp_log_errno(-r);
			goto err_cleanup;
		}
		/* auth_data */
		r = sd_bus_message_append(m, "s", "authData");
		if (r < 0) {
			fp_log_errno(-r);
			goto err_cleanup;
		}
		r = sd_bus_message_open_container(m, SD_BUS_TYPE_VARIANT, "ay");
		if (r < 0) {
			fp_log_errno(-r);
			goto err_cleanup;
		}
		r = sd_bus_message_append_array(m, 'y',
						fido_cred_authdata_ptr(closure->cred),
						fido_cred_authdata_len(closure->cred));
		if (r < 0) {
			fp_log_errno(-r);
			goto err_cleanup;
		}
		r = sd_bus_message_close_container(m);
		if (r < 0) {
			fp_log_errno(-r);
			goto err_cleanup;
		}
		r = sd_bus_message_close_container(m);
		if (r < 0) {
			fp_log_errno(-r);
			goto err_cleanup;
		}
		/* signature */
		r = sd_bus_message_open_container(m, SD_BUS_TYPE_DICT_ENTRY, "sv");
		if (r < 0) {
			fp_log_errno(-r);
			goto err_cleanup;
		}
		r = sd_bus_message_append(m, "s", "signature");
		if (r < 0) {
			fp_log_errno(r);
			goto err_cleanup;
		}
		r = sd_bus_message_open_container(m, SD_BUS_TYPE_VARIANT, "ay");
		if (r < 0) {
			fp_log_errno(r);
			goto err_cleanup;
		}
		r = sd_bus_message_append_array(m, 'y',
						fido_cred_sig_ptr(closure->cred),
						fido_cred_sig_len(closure->cred));
		if (r < 0) {
			fp_log_errno(r);
			goto err_cleanup;
		}
		r = sd_bus_message_close_container(m);
		if (r < 0) {
			fp_log_errno(r);
			goto err_cleanup;
		}
		r = sd_bus_message_close_container(m);
		if (r < 0) {
			fp_log_errno(-r);
			goto err_cleanup;
		}
		/* credential id */
		r = sd_bus_message_open_container(m, SD_BUS_TYPE_DICT_ENTRY, "sv");
		if (r < 0) {
			fp_log_errno(-r);
			goto err_cleanup;
		}
		r = sd_bus_message_append(m, "s", "credentialID");
		if (r < 0) {
			fp_log_errno(-r);
			goto err_cleanup;
		}
		r = sd_bus_message_open_container(m, SD_BUS_TYPE_VARIANT, "ay");
		if (r < 0) {
			fp_log_errno(-r);
			goto err_cleanup;
		}
		r = sd_bus_message_append_array(m, 'y',
						fido_cred_id_ptr(closure->cred),
						fido_cred_id_len(closure->cred));
		if (r < 0) {
			fp_log_errno(-r);
			goto err_cleanup;
		}
		r = sd_bus_message_close_container(m);
		if (r < 0) {
			fp_log_errno(-r);
			goto err_cleanup;
		}
		r = sd_bus_message_close_container(m);
		if (r < 0) {
			fp_log_errno(-r);
			goto err_cleanup;
		}
		/* public key */
		r = sd_bus_message_open_container(m, SD_BUS_TYPE_DICT_ENTRY, "sv");
		if (r < 0) {
			fp_log_errno(-r);
			goto err_cleanup;
		}
		r = sd_bus_message_append(m, "s", "publicKey");
		if (r < 0) {
			fp_log_errno(-r);
			goto err_cleanup;
		}
		r = sd_bus_message_open_container(m, SD_BUS_TYPE_VARIANT, "ay");
		if (r < 0) {
			fp_log_errno(-r);
			goto err_cleanup;
		}
		r = sd_bus_message_append_array(m, 'y',
						fido_cred_pubkey_ptr(closure->cred),
						fido_cred_pubkey_len(closure->cred));
		if (r < 0) {
			fp_log_errno(-r);
			goto err_cleanup;
		}
		r = sd_bus_message_close_container(m);
		if (r < 0) {
			fp_log_errno(-r);
			goto err_cleanup;
		}
		r = sd_bus_message_close_container(m);
		if (r < 0) {
			fp_log_errno(-r);
			goto err_cleanup;
		}
		/* x5c */
		r = sd_bus_message_open_container(m, SD_BUS_TYPE_DICT_ENTRY, "sv");
		if (r < 0) {
			fp_log_errno(-r);
			goto err_cleanup;
		}
		r = sd_bus_message_append(m, "s", "x5c");
		if (r < 0) {
			fp_log_errno(-r);
			goto err_cleanup;
		}
		r = sd_bus_message_open_container(m, SD_BUS_TYPE_VARIANT, "ay");
		if (r < 0) {
			fp_log_errno(-r);
			goto err_cleanup;
		}
		r = sd_bus_message_append_array(m, 'y',
						fido_cred_x5c_ptr(closure->cred),
						fido_cred_x5c_len(closure->cred));
		if (r < 0) {
			fp_log_errno(-r);
			goto err_cleanup;
		}
		r = sd_bus_message_close_container(m);
		if (r < 0) {
			fp_log_errno(-r);
			goto err_cleanup;
		}
		r = sd_bus_message_close_container(m);
		if (r < 0) {
			fp_log_errno(-r);
			goto err_cleanup;
		}
		/* fmt */
		r = sd_bus_message_open_container(m, SD_BUS_TYPE_DICT_ENTRY, "sv");
		if (r < 0) {
			fp_log_errno(-r);
			goto err_cleanup;
		}
		r = sd_bus_message_append(m, "s", "fmt");
		if (r < 0) {
			fp_log_errno(-r);
			goto err_cleanup;
		}
		r = sd_bus_message_open_container(m, SD_BUS_TYPE_VARIANT, "s");
		if (r < 0) {
			fp_log_errno(-r);
			goto err_cleanup;
		}
		r = sd_bus_message_append(m, "s", fido_cred_fmt(closure->cred));
		if (r < 0) {
			fp_log_errno(-r);
			goto err_cleanup;
		}
		r = sd_bus_message_close_container(m);
		if (r < 0) {
			fp_log_errno(-r);
			goto err_cleanup;
		}
		r = sd_bus_message_close_container(m);
		if (r < 0) {
			fp_log_errno(-r);
			goto err_cleanup;
		}
		/* clientdata hash */
		r = sd_bus_message_open_container(m, SD_BUS_TYPE_DICT_ENTRY, "sv");
		if (r < 0) {
			fp_log_errno(-r);
			goto err_cleanup;
		}
		r = sd_bus_message_append(m, "s", "clientdataHash");
		if (r < 0) {
			fp_log_errno(-r);
			goto err_cleanup;
		}
		r = sd_bus_message_open_container(m, SD_BUS_TYPE_VARIANT, "ay");
		if (r < 0) {
			fp_log_errno(-r);
			goto err_cleanup;
		}
		r = sd_bus_message_append_array(m, 'y',
						fido_cred_clientdata_hash_ptr(closure->cred),
						fido_cred_clientdata_hash_len(closure->cred));
		if (r < 0) {
			fp_log_errno(-r);
			goto err_cleanup;
		}
		r = sd_bus_message_close_container(m);
		if (r < 0) {
			fp_log_errno(-r);
			goto err_cleanup;
		}
		r = sd_bus_message_close_container(m);
		if (r < 0) {
			fp_log_errno(-r);
			goto err_cleanup;
		}
		r = sd_bus_message_close_container(m);
		if (r < 0) {
			fp_log_errno(-r);
			goto err_cleanup;
		}
	}

	not_cancel = true;
	return sd_bus_send(bus, m, NULL);

err_cleanup:
	not_cancel = true;
	pthread_mutex_trylock(&cancel_mutex);
	pthread_mutex_unlock(&cancel_mutex);
	close(closure->pp[0]);
	close(closure->pp[1]);

	return r;
}

static int
request_get_assertion(sd_bus *bus, const char *object_path, void *data)
{
	struct get_assert_closure *closure = data;
	_cleanup_(sd_bus_message_unrefp) sd_bus_message *m = NULL;
	int r;
	char pin[64];

	memset(pin, 0, 64);

	if (closure->uv == true) {
		r = sd_bus_message_new_signal(bus, &m, object_path,
					      FP_REQUEST_INTERFACE,
					      "PinRequired");
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}

		r = pipe2(closure->pp, O_CLOEXEC);
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}

		r = sd_bus_message_append(m, "h", closure->pp[1]);
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}

		r = sd_bus_send(bus, m, NULL);
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}

		if (not_cancel) {
			r = read(closure->pp[0], pin, 64);
		}
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}

		close(closure->pp[0]);
		close(closure->pp[1]);

		if (not_cancel) {
			r = fido_dev_get_assert(closure->dev, closure->assert, pin);
		}
	} else {
		if (not_cancel) {
			r = fido_dev_get_assert(closure->dev, closure->assert, NULL);
		}
	}

	if (!not_cancel) {
		r = sd_bus_message_new_signal(bus, &m, object_path,
					      FP_REQUEST_INTERFACE,
					      "Completed");
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}

		r = sd_bus_message_append(m, "s", "The request was cancelled");
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}

		return sd_bus_send(bus, m, NULL);
	} else if (r != FIDO_OK) {
		fp_error("failed to get assertion: %s", fido_strerr(r));
		r = sd_bus_message_new_signal(bus, &m, object_path,
					      FP_REQUEST_INTERFACE,
					      "Error");
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}
		r = sd_bus_message_append(m, "u", EINVAL);
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}
		r = sd_bus_message_append(m, "s", "Failed to get assert");
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}
	} else {
		size_t idx_count = fido_assert_count(closure->assert);

		r = sd_bus_message_new_signal(bus, &m, object_path,
					      FP_REQUEST_INTERFACE,
					      "Completed");
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}
		r = sd_bus_message_open_container(m, SD_BUS_TYPE_ARRAY, "{sv}");
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}

		for (size_t idx = 0; idx < idx_count; idx++) {
			/* count */
			r = sd_bus_message_open_container(m, SD_BUS_TYPE_DICT_ENTRY, "sv");
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = sd_bus_message_append(m, "s", "count");
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = sd_bus_message_open_container(m, SD_BUS_TYPE_VARIANT, "t");
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = sd_bus_message_append(m, "t", idx);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = sd_bus_message_close_container(m);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = sd_bus_message_close_container(m);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			/* authdata */
			r = sd_bus_message_open_container(m, SD_BUS_TYPE_DICT_ENTRY, "sv");
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = sd_bus_message_append(m, "s", "authData");
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = sd_bus_message_open_container(m, SD_BUS_TYPE_VARIANT, "ay");
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = sd_bus_message_append_array(m, 'y',
						fido_assert_authdata_ptr(closure->assert, idx),
						fido_assert_authdata_len(closure->assert, idx));
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = sd_bus_message_close_container(m);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = sd_bus_message_close_container(m);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			/* clientdata hash */
			r = sd_bus_message_open_container(m, SD_BUS_TYPE_DICT_ENTRY, "sv");
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = sd_bus_message_append(m, "s", "clientdataHash");
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = sd_bus_message_open_container(m, SD_BUS_TYPE_VARIANT, "ay");
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = sd_bus_message_append_array(m, 'y',
						fido_assert_clientdata_hash_ptr(closure->assert),
						fido_assert_clientdata_hash_len(closure->assert));
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = sd_bus_message_close_container(m);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = sd_bus_message_close_container(m);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			/* hmac secret */
			r = sd_bus_message_open_container(m, SD_BUS_TYPE_DICT_ENTRY, "sv");
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = sd_bus_message_append(m, "s", "hmacSecret");
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = sd_bus_message_open_container(m, SD_BUS_TYPE_VARIANT, "ay");
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = sd_bus_message_append_array(m, 'y',
						fido_assert_hmac_secret_ptr(closure->assert, idx),
						fido_assert_hmac_secret_len(closure->assert, idx));
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = sd_bus_message_close_container(m);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = sd_bus_message_close_container(m);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			/* user id */
			r = sd_bus_message_open_container(m, SD_BUS_TYPE_DICT_ENTRY, "sv");
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = sd_bus_message_append(m, "s", "userId");
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = sd_bus_message_open_container(m, SD_BUS_TYPE_VARIANT, "ay");
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = sd_bus_message_append_array(m, 'y',
						fido_assert_user_id_ptr(closure->assert, idx),
						fido_assert_user_id_len(closure->assert, idx));
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = sd_bus_message_close_container(m);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = sd_bus_message_close_container(m);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			/* user display name */
			r = sd_bus_message_open_container(m, SD_BUS_TYPE_DICT_ENTRY, "sv");
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = sd_bus_message_append(m, "s", "userDisplayName");
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = sd_bus_message_open_container(m, SD_BUS_TYPE_VARIANT, "s");
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = sd_bus_message_append(m, "s", fido_assert_user_display_name(closure->assert, idx));
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = sd_bus_message_close_container(m);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = sd_bus_message_close_container(m);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			/* user icon */
			r = sd_bus_message_open_container(m, SD_BUS_TYPE_DICT_ENTRY, "sv");
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = sd_bus_message_append(m, "s", "userIcon");
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = sd_bus_message_open_container(m, SD_BUS_TYPE_VARIANT, "s");
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = sd_bus_message_append(m, "s", fido_assert_user_icon(closure->assert, idx));
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = sd_bus_message_close_container(m);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = sd_bus_message_close_container(m);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			/* user name */
			r = sd_bus_message_open_container(m, SD_BUS_TYPE_DICT_ENTRY, "sv");
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = sd_bus_message_append(m, "s", "userName");
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = sd_bus_message_open_container(m, SD_BUS_TYPE_VARIANT, "s");
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = sd_bus_message_append(m, "s", fido_assert_user_name(closure->assert, idx));
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = sd_bus_message_close_container(m);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = sd_bus_message_close_container(m);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			/* signature */
			r = sd_bus_message_open_container(m, SD_BUS_TYPE_DICT_ENTRY, "sv");
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = sd_bus_message_append(m, "s", "signature");
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = sd_bus_message_open_container(m, SD_BUS_TYPE_VARIANT, "ay");
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = sd_bus_message_append_array(m, 'y',
						fido_assert_sig_ptr(closure->assert, idx),
						fido_assert_sig_len(closure->assert, idx));
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = sd_bus_message_close_container(m);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = sd_bus_message_close_container(m);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			/* signature count */
			r = sd_bus_message_open_container(m, SD_BUS_TYPE_DICT_ENTRY, "sv");
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = sd_bus_message_append(m, "s", "sigCount");
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = sd_bus_message_open_container(m, SD_BUS_TYPE_VARIANT, "u");
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = sd_bus_message_append(m, "u",
						fido_assert_sigcount(closure->assert, idx));
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = sd_bus_message_close_container(m);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			r = sd_bus_message_close_container(m);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
		}

		r = sd_bus_message_close_container(m);
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}
	}

	return sd_bus_send(bus, m, NULL);
}

static int
request_cancel(sd_bus_message *m, void *userdata, sd_bus_error *ret_error)
{
	//TODO remake this to be async
	struct fp_request *request = userdata;
	fido_dev_t *dev = NULL;

	pthread_kill(worker_thread, SIGUSR1);
	pthread_mutex_lock(&cancel_mutex);

	if (request->dispatch == request_make_cred) {
		struct make_cred_closure *closure = (struct make_cred_closure *)request->data;
		dev = closure->dev;
		if (closure->pp[1] != -1) {
			write(closure->pp[1], "\0", 1);
		}
	} else if (request->dispatch == request_get_assertion) {
		struct get_assert_closure *closure = (struct get_assert_closure *)request->data;
		dev = closure->dev;
		if (closure->pp[1] != -1) {
			write(closure->pp[1], "\0", 1);
		}
	} else {
		return -1;
	}

	fido_dev_cancel(dev);

	pthread_mutex_unlock(&cancel_mutex);

	return sd_bus_reply_method_return(m, NULL, NULL);
}

static const sd_bus_vtable request_vtable[] = {
	SD_BUS_VTABLE_START(0),
	SD_BUS_METHOD("Cancel", NULL, NULL, request_cancel, SD_BUS_VTABLE_UNPRIVILEGED),
	SD_BUS_SIGNAL_WITH_NAMES("Error",
				 "us", SD_BUS_PARAM(code) SD_BUS_PARAM(cause),
				 0),
	SD_BUS_SIGNAL_WITH_NAMES("Completed",
				 "a{sv}", SD_BUS_PARAM(result),
				 0),
	SD_BUS_SIGNAL_WITH_NAMES("PinRequired",
				 "h", SD_BUS_PARAM(pipe_write),
				 0),
	SD_BUS_VTABLE_END
};

static int
fp_server_enqueue_request(struct fp_server *server,
			  const char *request_object_path,
			  int (*dispatch)(sd_bus *, const char *, void *),
			  void *data, void (*data_free)(void *))
{
	_cleanup_(fp_request_freep) struct fp_request *request = NULL;
	int r;

	request = calloc(1, sizeof(struct fp_request));
	if (!request) {
		fp_log_errno(ENOMEM);
		return -ENOMEM;
	}

	request->object_path = strdup(request_object_path);
	request->data = data;
	request->data_free = data_free;
	request->dispatch = dispatch;

	r = sd_bus_add_object_vtable(server->bus,
				     &request->slot,
				     request->object_path,
				     FP_REQUEST_INTERFACE,
				     request_vtable, request);
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}

	if (!server->requests.head)
		server->requests.head = server->requests.tail = request;
	else
		server->requests.tail->next = request;
	request = NULL;

	server->requests.n_requests++;

	pthread_mutex_lock(&worker_mutex);
	pthread_cond_signal(&worker_cond);
	pthread_mutex_unlock(&worker_mutex);

	return 0;
}

static struct fp_request *
fp_server_dequeue_request(struct fp_server *server)
{
	struct fp_request *request;

	if (!server->requests.head)
		return NULL;

	if (server->requests.tail == server->requests.head)
		server->requests.tail = NULL;

	request = server->requests.head;
	server->requests.head = server->requests.head->next;

	server->requests.n_requests--;

	return request;
}

static void
make_cred_closure_free(void *data)
{
	struct make_cred_closure *closure = data;

	if (!closure)
		return;

	fido_dev_free(&closure->dev);
	fido_cred_free(&closure->cred);
	free(closure);
}

static int
make_credential(sd_bus_message *m, void *userdata, sd_bus_error *ret_error)
{
	int r;
	const char *type;
	uint8_t *client_data_hash;
	size_t client_data_hash_len;
	const char *rp;
	uint8_t *user_id;
	size_t user_id_len;
	const char *user_name;
	const char *user_display_name = NULL;
	const char *user_icon = NULL;
	struct fp_device *device = userdata;

	_cleanup_(fido_cred_free) fido_cred_t *cred = NULL;
	_cleanup_(fido_dev_free) fido_dev_t *dev = NULL;
	int cose_type;
	_cleanup_free_ char *request_object_path = NULL;
	struct make_cred_closure *closure;

	r = sd_bus_message_read_basic(m, 's', &type);
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}

	if (strcmp(type, "es256") == 0)
		cose_type = COSE_ES256;
	else if (strcmp(type, "rs256") == 0)
		cose_type = COSE_RS256;
	else if (strcmp(type, "eddsa") == 0)
		cose_type = COSE_EDDSA;
	else {
		fp_error("unknown type: \"%s\"", type);
		return sd_bus_reply_method_errnof(m, EINVAL,
						  "Unknown type: \"%s\"",
						  type);
	}

	r = sd_bus_message_read_array(m, 'y',
				      (const void **)&client_data_hash,
				      &client_data_hash_len);
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}

	r = sd_bus_message_read_basic(m, 's', &rp);
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}

	r = sd_bus_message_read_array(m, 'y', (const void **)&user_id, &user_id_len);
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}

	r = sd_bus_message_read_basic(m, 's', &user_name);
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}

	/* FIXME: check options */

	cred = fido_cred_new();
	if (!cred) {
		fp_error("failed to allocate fido_cred");
		return sd_bus_reply_method_errnof(m, ENOMEM, "Memory error");
	}

	/* REQUIRED ARGUMENTS */
	r = fido_cred_set_type(cred, cose_type);
	if (r != FIDO_OK) {
		fp_error("failed to set cred type: %s", fido_strerr(r));
		return sd_bus_reply_method_errnof(m, EINVAL,
						  "Failed to parse request");
	}

	r = fido_cred_set_clientdata_hash(cred, client_data_hash, client_data_hash_len);
	if (r != FIDO_OK) {
		fp_error("failed to set client data hash: %s", fido_strerr(r));
		return sd_bus_reply_method_errnof(m, EINVAL,
						  "Failed to parse request");
	}

	r = fido_cred_set_rp(cred, rp, NULL);
	if (r != FIDO_OK) {
		fp_error("failed to set rp: %s", fido_strerr(r));
		return sd_bus_reply_method_errnof(m, EINVAL,
						  "Failed to parse request");
	}

	r = fido_cred_set_user(cred, user_id, user_id_len, user_name, user_display_name, user_icon);
	if (r != FIDO_OK) {
		fp_error("failed to set user: %s", fido_strerr(r));
		return sd_bus_reply_method_errnof(m, EINVAL,
						  "Failed to parse request");
	}

	/* REQUIRED ARGUMENTS END */

	/* OPTIONAL ARGUMENTS */
	r = sd_bus_message_enter_container(m, 'a', "{sv}");
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}

	for (;;) {
		const char *label;

		r = sd_bus_message_enter_container(m, 'e', "sv");
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}
		if (r == 0){
			break;
		}

		r = sd_bus_message_read_basic(m, 's', &label);
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}

		if (streq(label, "authdata")) {
			uint8_t *authdata_ptr;
			size_t authdata_len;

			r = sd_bus_message_enter_container(m, 'v', "ay");
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}

			r = sd_bus_message_read_array(m, 'y',
							(const void**)&authdata_ptr,
							&authdata_len);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}

			r = fido_cred_set_authdata(cred, authdata_ptr, authdata_len);
			if (r != FIDO_OK) {
				fp_error("failed to set authdata: %s", fido_strerr(r));
				return sd_bus_reply_method_errnof(m, EINVAL,
								  "Failed to parse request");
			}

			r = sd_bus_message_exit_container(m);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}

		} else if (streq(label, "authdataRaw")) {
			uint8_t *authdata_raw_ptr;
			size_t authdata_raw_len;

			r = sd_bus_message_enter_container(m, 'v', "ay");
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}

			r = sd_bus_message_read_array(m, 'y',
							(const void**)&authdata_raw_ptr,
							&authdata_raw_len);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}

			r = fido_cred_set_authdata_raw(cred, authdata_raw_ptr, authdata_raw_len);
			if (r != FIDO_OK) {
				fp_error("failed to set raw authdata: %s", fido_strerr(r));
				return sd_bus_reply_method_errnof(m, EINVAL,
								  "Failed to parse request");
			}

			r = sd_bus_message_exit_container(m);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			
		} else if (streq(label, "x5c")) {
			uint8_t *x5c_ptr;
			size_t x5c_len;

			r = sd_bus_message_enter_container(m, 'v', "ay");
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}

			r = sd_bus_message_read_array(m, 'y',
							(const void**)&x5c_ptr,
							&x5c_len);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}

			r = fido_cred_set_x509(cred, x5c_ptr, x5c_len);
			if (r != FIDO_OK) {
				fp_error("failed to set x5c: %s", fido_strerr(r));
				return sd_bus_reply_method_errnof(m, EINVAL,
								  "Failed to parse request");
			}

			r = sd_bus_message_exit_container(m);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			
		} else if (streq(label, "signature")) {
			uint8_t *sig_ptr;
			size_t sig_len;

			r = sd_bus_message_enter_container(m, 'v', "ay");
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}

			r = sd_bus_message_read_array(m, 'y',
							(const void**)&sig_ptr,
							&sig_len);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}

			r = fido_cred_set_sig(cred, sig_ptr, sig_len);
			if (r != FIDO_OK) {
				fp_error("failed to set signature: %s", fido_strerr(r));
				return sd_bus_reply_method_errnof(m, EINVAL,
								  "Failed to parse request");
			}

			r = sd_bus_message_exit_container(m);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			
		} else if (streq(label, "extensions")) {
			uint8_t ext;

			r = sd_bus_message_read(m, "v", "y", &ext);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}

			switch (ext) {
				case FIDO_EXT_HMAC_SECRET:
					r = fido_cred_set_extensions(cred, FIDO_EXT_HMAC_SECRET);
					break;
				/*case FIDO_EXT_CRED_PROTECT:
					r = fido_cred_set_extensions(cred, FIDO_EXT_CRED_PROTECT);
					break;*/
				default:
					r = fido_cred_set_extensions(cred, 0);
					break;
			}
			if (r != FIDO_OK) {
				fp_error("failed to set extensions: %s", fido_strerr(r));
				return sd_bus_reply_method_errnof(m, EINVAL,
								  "Failed to parse request");
			}
			
		} /*else if (streq(label, "protection")) {
			int prot;

			r = sd_bus_message_read(m, "v", "i", &prot);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}

			r = fido_cred_set_prot(cred, prot);
			if (r != FIDO_OK) {
				fp_error("failed to set protection: %s", fido_strerr(r));
				return sd_bus_reply_method_errnof(m, EINVAL,
								  "Failed to parse request");
			}
			
		}*/ else if (streq(label, "rk")) {
			uint8_t rk;

			r = sd_bus_message_read(m, "v", "y", &rk);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}

			switch (rk) {
				case FIDO_OPT_OMIT:
					r = fido_cred_set_rk(cred, FIDO_OPT_OMIT);
					if (r != FIDO_OK) {
						fp_error("failed to set resident key: %s", fido_strerr(r));
						return sd_bus_reply_method_errnof(m, EINVAL,
										  "Failed to parse request");
					}
					break;
				case FIDO_OPT_TRUE:
					r = fido_cred_set_rk(cred, FIDO_OPT_TRUE);
					if (r != FIDO_OK) {
						fp_error("failed to set resident key: %s", fido_strerr(r));
						return sd_bus_reply_method_errnof(m, EINVAL,
										  "Failed to parse request");
					}
					break;
				case FIDO_OPT_FALSE:
					r = fido_cred_set_rk(cred, FIDO_OPT_FALSE);
					if (r != FIDO_OK) {
						fp_error("failed to set resident key: %s", fido_strerr(r));
						return sd_bus_reply_method_errnof(m, EINVAL,
										  "Failed to parse request");
					}
					break;
				default:
					fp_log_errno(-EINVAL);
					return sd_bus_reply_method_errnof(m, EINVAL,
									  "Failed to parse request");
			}
		} else if (streq(label, "uv")) {
			uint8_t uv;

			r = sd_bus_message_read(m, "v", "y", &uv);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}

			switch (uv) {
				case FIDO_OPT_OMIT:
					r = fido_cred_set_uv(cred, FIDO_OPT_OMIT);
					if (r != FIDO_OK) {
						fp_error("failed to set user verification: %s", fido_strerr(r));
						return sd_bus_reply_method_errnof(m, EINVAL,
										  "Failed to parse request");
					}
					break;
				case FIDO_OPT_TRUE:
					r = fido_cred_set_uv(cred, FIDO_OPT_TRUE);
					if (r != FIDO_OK) {
						fp_error("failed to set user verification: %s", fido_strerr(r));
						return sd_bus_reply_method_errnof(m, EINVAL,
										  "Failed to parse request");
					}
					break;
				case FIDO_OPT_FALSE:
					r = fido_cred_set_uv(cred, FIDO_OPT_FALSE);
					if (r != FIDO_OK) {
						fp_error("failed to set user verification: %s", fido_strerr(r));
						return sd_bus_reply_method_errnof(m, EINVAL,
										  "Failed to parse request");
					}
					break;
				default:
					fp_log_errno(-EINVAL);
					return sd_bus_reply_method_errnof(m, EINVAL,
									  "Failed to parse request");
			}
		} else if (streq(label, "fmt")) {
			char *fmt;

			r = sd_bus_message_read(m, "v", "s", &fmt);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}

			r = fido_cred_set_fmt(cred, fmt);
			if (r != FIDO_OK) {
				fp_error("failed to set protection: %s", fido_strerr(r));
				return sd_bus_reply_method_errnof(m, EINVAL,
								  "Failed to parse request");
			}
			
		} else if (streq(label, "userDisplayName")) {
			r = sd_bus_message_read(m, "v", "s", &user_display_name);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			
		} else if (streq(label, "userIcon")) {
			r = sd_bus_message_read(m, "v", "s", &user_icon);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			
		} else {
			r = sd_bus_message_skip(m, "v");
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
		}

		r = sd_bus_message_exit_container(m);
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}
	}


	r = sd_bus_message_exit_container(m);
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}

	/* OPTIONAL ARGUMENTS END */

	dev = fido_dev_new();
	if (!dev) {
		fp_error("failed to allocate fido_dev");
		return sd_bus_reply_method_errnof(m, ENOMEM, "Memory error");
	}

	r = fido_dev_open(dev, device->path);
	if (r != FIDO_OK) {
		fp_error("failed to open device: %s", fido_strerr(r));
		return sd_bus_reply_method_errnof(m, EINVAL,
						  "Failed to parse request");
	}

	closure = calloc(1, sizeof(struct make_cred_closure));
	if (!closure) {
		fp_error("failed to allocate make_cred_closure");
		return sd_bus_reply_method_errnof(m, ENOMEM, "Memory error");
	}

	if (asprintf(&request_object_path, FP_REQUEST_PATH "/%lu",
		     device->server->requests.n_requests) < 0) {
		fp_error("failed to allocate request_object_path");
		return sd_bus_reply_method_errnof(m, ENOMEM, "Memory error");
	}

	closure->dev = dev;
	dev = NULL;
	closure->cred = cred;
	cred = NULL;
	closure->pp[0] = -1;
	closure->pp[1] = -1;

	r = fp_server_enqueue_request(device->server, request_object_path,
				      request_make_cred, closure,
				      make_cred_closure_free);
	if (r < 0) {
		fp_error("failed to enqueue request");
		return sd_bus_reply_method_errnof(m, ENOMEM, "Memory error");
	}

	return sd_bus_reply_method_return(m, "o", request_object_path);
}

static void
get_assert_closure_free(void *data)
{
	struct get_assert_closure *closure = data;

	if (!closure)
		return;

	fido_dev_free(&closure->dev);
	fido_assert_free(&closure->assert);
	free(closure);
}

static int
get_assertion(sd_bus_message *m, void *userdata, sd_bus_error *ret_error)
{
	int r;
	uint8_t *client_data_hash;
	size_t client_data_hash_len;
	uint8_t *allow_cred;
	size_t allow_cred_len;
	const char *rp;
	struct fp_device *device = userdata;

	_cleanup_(fido_assert_free) fido_assert_t *assert = NULL;
	_cleanup_(fido_dev_free) fido_dev_t *dev = NULL;
	_cleanup_free_ char *request_object_path = NULL;
	struct get_assert_closure *closure;

	closure = calloc(1, sizeof(struct get_assert_closure));
	if (!closure) {
		fp_error("failed to allocate get_assert_closure");
		return sd_bus_reply_method_errnof(m, ENOMEM, "Memory error");
	}
	
	closure->uv = false;

	r = sd_bus_message_read_array(m, 'y', 
					(const void **)&client_data_hash,
					&client_data_hash_len);
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}

	r = sd_bus_message_read_basic(m, 's', &rp);
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}

	r = sd_bus_message_read_array(m, 'y', 
					(const void **)&allow_cred,
					&allow_cred_len);
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}

	assert = fido_assert_new();
	if (!assert) {
		fp_error("failed to allocate fido_assert");
		return sd_bus_reply_method_errnof(m, ENOMEM, "Memory error");
	}

	/* REQUIRED ARGUMENTS */
	r = fido_assert_set_clientdata_hash(assert, client_data_hash, client_data_hash_len);
	if (r != FIDO_OK) {
		fp_error("failed to set client data hash: %s", fido_strerr(r));
		return sd_bus_reply_method_errnof(m, EINVAL,
						  "Failed to parse request");
	}

	r = fido_assert_set_rp(assert, rp);
	if (r != FIDO_OK) {
		fp_error("failed to set rp: %s", fido_strerr(r));
		return sd_bus_reply_method_errnof(m, EINVAL,
						  "Failed to parse request");
	}

	r = fido_assert_allow_cred(assert, allow_cred, allow_cred_len);
	if (r != FIDO_OK) {
		fp_error("failed to allow credential: %s", fido_strerr(r));
		return sd_bus_reply_method_errnof(m, EINVAL,
						  "Failed to parse request");
	}
	/* REQUIRED ARGUMENTS END */

	/* OPTIONAL ARGUMENTS */
	r = sd_bus_message_enter_container(m, 'a', "{sv}");
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}

	for (;;) {
		const char *label;

		r = sd_bus_message_enter_container(m, 'e', "sv");
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}
		if (r == 0) {
			break;
		}

		r = sd_bus_message_read_basic(m, 's', &label);
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}
		
		/*if (streq(label, "authdata")) {
			uint8_t *authdata_ptr;
			size_t authdata_len;

			r = sd_bus_message_enter_container(m, 'v', "ay");
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}

			r = sd_bus_message_read_array(m, 'y',
							(const void **)&authdata_ptr,
							&authdata_len);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}

			r = fido_assert_set_authdata(assert, 0, authdata_ptr, authdata_len);
			if (r != FIDO_OK) {
				fp_error("failed to set authdata: %s", fido_strerr(r));
				return sd_bus_reply_method_errnof(m, EINVAL,
								  "Failed to parse request");
			}

			r = sd_bus_message_exit_container(m);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}

		} else if (streq(label, "authdataRaw")) {
			uint8_t *authdata_raw_ptr;
			size_t authdata_raw_len;

			r = sd_bus_message_enter_container(m, 'v', "ay");
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}

			r = sd_bus_message_read_array(m, 'y',
							(const void **)&authdata_raw_ptr,
							&authdata_raw_len);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}

			// TODO which index to use below??
			r = fido_assert_set_authdata_raw(assert, 0, authdata_raw_ptr, authdata_raw_len);
			if (r != FIDO_OK) {
				fp_error("failed to set raw authdata: %s", fido_strerr(r));
				return sd_bus_reply_method_errnof(m, EINVAL,
								  "Failed to parse request");
			}

			r = sd_bus_message_exit_container(m);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}

		} else */if (streq(label, "count")) {
			size_t n;

			r = sd_bus_message_read(m, "v", "t", &n);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}

			r = fido_assert_set_count(assert, n);
			if (r != FIDO_OK) {
				fp_error("failed to set count: %s", fido_strerr(r));
				return sd_bus_reply_method_errnof(m, EINVAL,
								  "Failed to parse request");
			}

		} else if (streq(label, "extensions")) {
			uint8_t ext;

			r = sd_bus_message_read(m, "v", "y", &ext);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}

			switch (ext) {
				case FIDO_EXT_HMAC_SECRET:
					r = fido_assert_set_extensions(assert, FIDO_EXT_HMAC_SECRET);
					break;
				/*case FIDO_EXT_CRED_PROTECT:
					r = fido_assert_set_extensions(assert, FIDO_EXT_CRED_PROTECT);
					break;*/
				default:
					r = fido_assert_set_extensions(assert, 0);
					break;
			}
			if (r != FIDO_OK) {
				fp_error("failed to set extensions: %s", fido_strerr(r));
				return sd_bus_reply_method_errnof(m, EINVAL,
								  "Failed to parse request");
			}

		} else if (streq(label, "hmacSalt")) {
			uint8_t *hmac_ptr;
			size_t hmac_len;

			r = sd_bus_message_enter_container(m, 'v', "ay");
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}

			r = sd_bus_message_read_array(m, 'y',
							(const void **)&hmac_ptr,
							&hmac_len);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}

			r = fido_assert_set_hmac_salt(assert, hmac_ptr, hmac_len);
			if (r != FIDO_OK) {
				fp_error("failed to set hmac salt: %s", fido_strerr(r));
				return sd_bus_reply_method_errnof(m, EINVAL,
								  "Failed to parse request");
			}

			r = sd_bus_message_exit_container(m);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}

		} else if (streq(label, "up")) {
			uint8_t up;

			r = sd_bus_message_read(m, "v", "y", &up);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			
			switch (up) {
				case FIDO_OPT_OMIT:
					r = fido_assert_set_up(assert, FIDO_OPT_OMIT);
					if (r != FIDO_OK) {
						fp_error("failed to set up: %s", fido_strerr(r));
						return sd_bus_reply_method_errnof(m, EINVAL,
										  "Failed to parse request");
					}
					break;
				case FIDO_OPT_TRUE:
					r = fido_assert_set_up(assert, FIDO_OPT_TRUE);
					if (r != FIDO_OK) {
						fp_error("failed to set up: %s", fido_strerr(r));
						return sd_bus_reply_method_errnof(m, EINVAL,
										  "Failed to parse request");
					}
					break;
				case FIDO_OPT_FALSE:
					r = fido_assert_set_up(assert, FIDO_OPT_FALSE);
					if (r != FIDO_OK) {
						fp_error("failed to set up: %s", fido_strerr(r));
						return sd_bus_reply_method_errnof(m, EINVAL,
										  "Failed to parse request");
					}
					break;
				default:
					fp_error("failed to set up: %s", fido_strerr(r));
					return sd_bus_reply_method_errnof(m, EINVAL,
									  "Failed to parse request");
			}
		} else if (streq(label, "uv")) {
			uint8_t uv;

			r = sd_bus_message_read(m, "v", "y", &uv);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
			
			switch (uv) {
				case FIDO_OPT_OMIT:
					r = fido_assert_set_uv(assert, FIDO_OPT_OMIT);
					if (r != FIDO_OK) {
						fp_error("failed to set uv: %s", fido_strerr(r));
						return sd_bus_reply_method_errnof(m, EINVAL,
										  "failed to parse request");
					}
					break;
				case FIDO_OPT_TRUE:
					r = fido_assert_set_uv(assert, FIDO_OPT_TRUE);
					if (r != FIDO_OK) {
						fp_error("failed to set uv: %s", fido_strerr(r));
						return sd_bus_reply_method_errnof(m, EINVAL,
										  "failed to parse request");
					}
					closure->uv = true;
					break;
				case FIDO_OPT_FALSE:
					r = fido_assert_set_uv(assert, FIDO_OPT_FALSE);
					if (r != FIDO_OK) {
						fp_error("failed to set uv: %s", fido_strerr(r));
						return sd_bus_reply_method_errnof(m, EINVAL,
										  "failed to parse request");
					}
					break;
				default:
					fp_error("failed to set uv: %s", fido_strerr(r));
					return sd_bus_reply_method_errnof(m, EINVAL,
									  "failed to parse request");
			}
		} /*else if (streq(label, "signature")) {
			uint8_t *sig_ptr;
			size_t sig_len;

			r = sd_bus_message_enter_container(m, 'v', "ay");
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}

			r = sd_bus_message_read_array(m, 'y',
							(const void **)&sig_ptr,
							&sig_len);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}

			r = fido_assert_set_sig(assert, 0, sig_ptr, sig_len);
			if (r != FIDO_OK) {
				fp_error("failed to set sig: %s", fido_strerr(r));
				return sd_bus_reply_method_errnof(m, EINVAL,
								  "Failed to parse request");
			}

			r = sd_bus_message_exit_container(m);
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}

		} */else {
			r = sd_bus_message_skip(m, "v");
			if (r < 0) {
				fp_log_errno(-r);
				return r;
			}
		}
		
		r = sd_bus_message_exit_container(m);
		if (r < 0) {
			fp_log_errno(-r);
			return r;
		}
	}

	r = sd_bus_message_exit_container(m);
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}

	/* OPTIONAL ARGUMENTS END */

	dev = fido_dev_new();
	if (!dev) {
		fp_error("failed to allocate fido_dev");
		return sd_bus_reply_method_errnof(m, ENOMEM, "Memory error");
	}

	r = fido_dev_open(dev, device->path);
	if (r != FIDO_OK) {
		fp_error("failed to open device: %s", fido_strerr(r));
		return sd_bus_reply_method_errnof(m, EINVAL,
						  "Failed to parse request");
	}

	if (asprintf(&request_object_path, FP_REQUEST_PATH "/%lu",
		     device->server->requests.n_requests) < 0) {
		fp_error("failed to allocate request_object_path");
		return sd_bus_reply_method_errnof(m, ENOMEM, "Memory error");
	}

	closure->dev = dev;
	dev = NULL;
	closure->assert = assert;
	assert = NULL;
	closure->pp[0] = -1;
	closure->pp[1] = -1;

	r = fp_server_enqueue_request(device->server, request_object_path,
				      request_get_assertion, closure,
				      get_assert_closure_free);
	if (r < 0) {
		fp_error("failed to enqueue request");
		return sd_bus_reply_method_errnof(m, ENOMEM, "Memory error");
	}

	return sd_bus_reply_method_return(m, "o", request_object_path);
}

static const sd_bus_vtable device_vtable[] = {
	SD_BUS_VTABLE_START(0),
	SD_BUS_PROPERTY("Path", "s", NULL, offsetof(struct fp_device, path),
			SD_BUS_VTABLE_PROPERTY_CONST),
	SD_BUS_METHOD_WITH_NAMES("MakeCredential", "saysaysa{sv}", SD_BUS_PARAM(type) SD_BUS_PARAM(client_data_hash) SD_BUS_PARAM(rp) SD_BUS_PARAM(user_id) SD_BUS_PARAM(user_name) SD_BUS_PARAM(options), "o", SD_BUS_PARAM(handle),
				 make_credential, SD_BUS_VTABLE_UNPRIVILEGED),
	SD_BUS_METHOD_WITH_NAMES("GetAssertion",
				 "aysaya{sv}", SD_BUS_PARAM(client_data_hash) SD_BUS_PARAM(rp) SD_BUS_PARAM(allow_cred) SD_BUS_PARAM(options),
				 "o", SD_BUS_PARAM(handle),
				 get_assertion, SD_BUS_VTABLE_UNPRIVILEGED),
	SD_BUS_VTABLE_END
};

static struct fp_device *
fp_server_find_device(struct fp_server *server,
		      struct udev_device *d)
{
	struct fp_device *p;
	const char *path;

	if (!d)
		return NULL;

	path = udev_device_get_devnode(d);
	if (!path)
		return NULL;

	for (p = server->devices; p; p = p->next)
		if (strcmp(p->path, path) == 0)
			return p;

	return NULL;
}

static void
fp_device_free(struct fp_device *device)
{
	if (!device)
		return;

	sd_bus_slot_unref(device->slot);
	free(device->path);
	free(device->object_path);
	free(device);
}

static inline void
fp_device_freep(void *p)
{
	fp_device_free(*(struct fp_device **)p);
}

static void
fp_server_remove_device(struct fp_server *server, struct fp_device *device)
{
	if (!device)
		return;

	if (server->devices == device) {
		server->devices = device->next;
		if (device->next)
			device->next->prev = NULL;
	} else {
		if (device->next)
			device->next->prev = device->prev;
		if (device->prev)
			device->prev->next = device->next;
	}

	server->n_devices--;

	sd_bus_slot_unrefp(&device->slot);
	sd_bus_emit_object_removed(server->bus, device->object_path);

	fp_device_free(device);
}

static void
fp_server_move_device(struct fp_server *server,
		      struct fp_device *device,
		      struct udev_device *d)
{
	const char *path;

	if (!device)
		return;

	path = udev_device_get_devnode(d);
	if (!path)
		return;

	free(device->path);
	device->path = strdup(path);
}

static int
add_device(struct fp_server *server, struct udev_device *d, bool emit_signal)
{
	_cleanup_(fp_device_freep) struct fp_device *device = NULL;
	const char *path;
	int r;

	path = udev_device_get_devnode(d);
	if (!path)
		return -EINVAL;

	if (!is_fido(path))
		return 0;

	device = calloc(1, sizeof(struct fp_device));
	if (!device)
		return -ENOMEM;

	device->server = server;

	if (asprintf(&device->object_path, FP_DEVICE_PATH "/%lu",
		     server->n_devices) < 0)
		return -ENOMEM;

	device->path = strdup(path);
	if (!device->path)
		return -ENOMEM;

	r = sd_bus_add_object_vtable(server->bus, &device->slot,
				     device->object_path,
				     FP_DEVICE_INTERFACE,
				     device_vtable, device);
	if (r < 0)
		return r;

	if (emit_signal) {
		r = sd_bus_emit_object_added(server->bus, device->object_path);
		if (r < 0)
			return r;
	}

	device->next = server->devices;
	if (server->devices)
		server->devices->prev = device;
	server->devices = device;
	device = NULL;

	server->n_devices++;

	return 0;
}

static int
fp_server_add_device(struct fp_server *server, struct udev_device *d)
{
	return add_device(server, d, true);
}

static inline void
udev_device_unrefp(void *p)
{
	struct udev_device *device = *(struct udev_device **)p;
	if (device)
		udev_device_unref(device);
}

static int
fp_udev_monitor(sd_event_source *source,
		int fd,
		uint32_t mask,
		void *data)
{
	struct fp_server *server = data;
	struct fp_device *device;
	_cleanup_(udev_device_unrefp) struct udev_device *d = NULL;
	const char *action;

	d = udev_monitor_receive_device(server->udev_monitor);
	if (!d)
		return 0;

	device = fp_server_find_device(server, d);
	action = udev_device_get_action(d);

	if (action && strcmp(action, "remove") == 0)
		fp_server_remove_device(server, device);
	else if (action && strcmp(action, "move") == 0)
		fp_server_move_device(server, device, d);
	else
		fp_server_add_device(server, d);

	return 0;
}

static int
fp_server_init(struct fp_server *server)
{
	int r;

	memset(server, 0, sizeof(struct fp_server));

	r = sd_event_default(&server->event);
	if (r < 0)
		return r;

	r = sd_bus_open_user(&server->bus);
	if (r < 0)
		return r;

	sd_bus_attach_event(server->bus, server->event,
			    SD_EVENT_PRIORITY_NORMAL);

	server->udev = udev_new();
	if (!server->udev)
		return -ENOMEM;

	server->udev_monitor = udev_monitor_new_from_netlink(server->udev, "udev");
	if (!server->udev_monitor)
		return -ENOMEM;

	r = udev_monitor_filter_add_match_subsystem_devtype(server->udev_monitor,
							    "hidraw", NULL);
	if (r < 0)
		return r;

	r = udev_monitor_enable_receiving(server->udev_monitor);
	if (r < 0)
		return r;

	r = sd_event_add_io(server->event,
			    &server->udev_monitor_source,
			    udev_monitor_get_fd(server->udev_monitor),
			    EPOLLHUP | EPOLLERR | EPOLLIN,
			    fp_udev_monitor,
			    server);
	if (r < 0)
		return r;

	return 0;
}

static inline void
udev_enumerate_unrefp(void *p)
{
	struct udev_enumerate *enumerate = *(struct udev_enumerate **)p;
	if (enumerate)
		udev_enumerate_unref(enumerate);
}

static int
fp_server_enumerate_devices(struct fp_server *server)
{
	_cleanup_(udev_enumerate_unrefp) struct udev_enumerate *enumerate = NULL;
	struct udev_list_entry *devices, *dev_list_entry;
	int r;

	enumerate = udev_enumerate_new(server->udev);
	if (!enumerate)
		return -ENOMEM;

	udev_enumerate_add_match_subsystem(enumerate, "hidraw");
	r = udev_enumerate_scan_devices(enumerate);
	if (r < 0)
		return r;

	devices = udev_enumerate_get_list_entry(enumerate);
	if (!devices)
		return 0;

	udev_list_entry_foreach(dev_list_entry, devices){
		struct udev_device *d;
		const char *path;

		path = udev_list_entry_get_name(dev_list_entry);
		d = udev_device_new_from_syspath(server->udev, path);

		r = add_device(server, d, 0);
		if (r < 0)
			fp_error("failed to add device: %s", path);
		udev_device_unref(d);
	}

	return 0;
}

static int
enumerate_devices(sd_bus *bus, const char *path, void *userdata, char ***nodes,
		  sd_bus_error *error)
{
	struct fp_server *server = userdata;
	struct fp_device *p;
	char **object_paths;
	size_t i;

	object_paths = calloc(server->n_devices + 1, sizeof(char *));
	if (!object_paths) {
		fp_log_errno(ENOMEM);
		return -ENOMEM;
	}

	for (i = 0, p = server->devices; p; p = p->next)
		object_paths[i] = strdup(p->object_path);

	*nodes = object_paths;
        return 1;
}

static void
fp_server_destroy(struct fp_server *server)
{
	struct fp_device *device;
	struct fp_request *request;

	sd_bus_detach_event(server->bus);
	sd_bus_close(server->bus);
	sd_bus_unref(server->bus);

	sd_event_unref(server->event);

	for (device = server->devices; device; ) {
		struct fp_device *next = device->next;
		fp_device_free(device);
		device = next;
	}

	for (request = server->requests.head; request; ) {
		struct fp_request *next = request->next;
		fp_request_free(request);
		request = next;
	}

	udev_monitor_unref(server->udev_monitor);
	udev_unref(server->udev);
}

static int
fp_handle_server_sigterm(sd_event_source *s, const struct signalfd_siginfo *si, void *userdata) {
	struct fp_server *server = userdata;

	pthread_cond_signal(&worker_cond);
	pthread_kill(worker_thread, SIGTERM);
	sd_event_exit(server->event, 0);

	return 0;
}

static int
fp_server_startup(struct fp_server *server)
{
	int r;

	r = sd_bus_request_name(server->bus, FP_SERVICE_NAME, 0);
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}

	fp_server_enumerate_devices(server);

	r = sd_bus_add_node_enumerator(server->bus, NULL, FP_DEVICE_PATH,
				       enumerate_devices, server);
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}

	r = sd_bus_add_object_manager(server->bus, NULL, FP_DEVICE_PATH);
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}

	sigset_t set;
	sigemptyset(&set);
	sigaddset(&set, SIGTERM);

	r = sigprocmask(SIG_BLOCK, &set, NULL);
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}

	r = sd_event_add_signal(server->event, NULL, SIGTERM, fp_handle_server_sigterm, server);
	if (r < 0) {
		fp_log_errno(-r);
		return r;
	}

	return 0;
}

static int
fp_server_run(struct fp_server *server)
{
	return sd_event_loop(server->event);
}

static void *
worker(void *data)
{
	signal(SIGUSR1, signal_handler);
	not_cancel = true;
	struct fp_server *server = data;

	pthread_mutex_lock(&worker_mutex);
	while (server->requests.n_requests == 0) {
		struct fp_request *request;
		int r;

		pthread_cond_wait(&worker_cond, &worker_mutex);

		request = fp_server_dequeue_request(server);
		if (!request)
			break;

		r = request->dispatch(server->bus, request->object_path, request->data);
		if (r < 0)
			fp_log_errno(-r);

		fp_request_free(request);
	}
	pthread_mutex_unlock(&worker_mutex);

	return NULL;
}

int
main(void)
{
	//DEBUG_START
	//fido_init(FIDO_DEBUG);
	//DEBUG_END
	struct fp_server server;
	int status = EXIT_SUCCESS;
	int r;

	r = fp_server_init(&server);
	if (r < 0) {
		fprintf(stderr, "Failed to initialize server\n");
		status = EXIT_FAILURE;
		goto out;
	}

	r = fp_server_startup(&server);
	if (r < 0) {
		fprintf(stderr, "Failed to startup server\n");
		status = EXIT_FAILURE;
		goto out;
	}

	r = pthread_create(&worker_thread, NULL, worker, &server);
	if (r < 0) {
		fprintf(stderr, "Failed to create worker thread\n");
		status = EXIT_FAILURE;
		goto out;
	}

	r = fp_server_run(&server);
	if (r < 0) {
		fprintf(stderr, "Failed to run server\n");
		status = EXIT_FAILURE;
		goto out;
	}

 out:
	fp_server_destroy(&server);
	pthread_join(worker_thread, NULL);

	return status;
}
